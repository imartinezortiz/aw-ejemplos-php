<?php

require_once __DIR__.'/includes/Usuario.php';
require_once __DIR__.'/includes/Formulario.php';
require_once __DIR__.'/includes/FormularioAjax.php';

class PruebaFormAjax extends FormularioAjax
{

    public function __construct() {
        parent::__construct('formLoginAjax');
    }

    protected function generaCamposFormulario(&$datos)
    {
        // Se reutiliza el nombre de usuario introducido previamente o se deja en blanco
        $nombreUsuario = $datos['nombreUsuario'] ?? '';

        // Se generan los mensajes de error si existen.
        $htmlErroresGlobales = self::generaListaErroresGlobales($this->errores);
        $erroresCampos = self::generaErroresCampos(['nombreUsuario', 'password'], $this->errores, 'span', array('class' => 'error'));

        // Se genera el HTML asociado a los campos del formulario y los mensajes de error.
        $html = <<<EOF
        $htmlErroresGlobales
        <fieldset>
            <legend>Usuario y contraseña</legend>
            <div>
                <label for="nombreUsuario">Nombre de usuario:</label>
                <input id="nombreUsuario" type="text" name="nombreUsuario" value="$nombreUsuario" />
                {$erroresCampos['nombreUsuario']}
            </div>
            <div>
                <label for="password">Password:</label>
                <input id="password" type="password" name="password" />
                {$erroresCampos['password']}
            </div>
            <div>
                <button type="submit" name="login">Entrar</button>
            </div>
        </fieldset>
        EOF;
        return $html;
    }

    protected function procesaFormulario(&$datos)
    {
        $this->errores = [];
        $nombreUsuario = trim($datos['nombreUsuario'] ?? '');
        $nombreUsuario = filter_var($nombreUsuario, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        if ( ! $nombreUsuario || empty($nombreUsuario) ) {
            $this->errores['nombreUsuario'] = 'El nombre de usuario no puede estar vacío';
        }
        
        $password = trim($datos['password'] ?? '');
        $password = filter_var($password, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        if ( ! $password || empty($password) ) {
            $this->errores['password'] = 'El password no puede estar vacío.';
        }
        
        if (count($this->errores) === 0) {
            $usuario = Usuario::login($nombreUsuario, $password);
        
            if (!$usuario) {
                $this->errores[] = "El usuario o el password no coinciden";
            } else {
                $result = new stdClass();
                $result->login = true;
                $result->nombre = $nombreUsuario;
                $result->esAdmin = $usuario->tieneRol(ADMIN_ROLE);
                return $result;
            }
        }
    }
}

$formulario = new PruebaFormAjax();

$htmlFormulario = $formulario->gestiona();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Ejemplo de Formulario AJAX</h1>
    <?= $htmlFormulario ?>
    <div id="result"></div>
    <script>
        let form = document.getElementById('formLoginAjax');
        let result = document.getElementById('result');
        form.addEventListener('submit', (e) => {
            e.preventDefault();

            let data = new FormData(form);

            fetch(window.location.href, {
                method: 'POST',
                body: new FormData(event.target)
            })
//            .then(response => response.ok ? response.json() : Promise.reject(response))
            .then(response => response.json())
            .then((data) => {
                result.innerHTML = `<pre><code>${JSON.stringify(data)}</code></pre>`;
            })
            .catch((response) => {
                result.innerHTML = `Oops`;
            })
        })
        
    </script>
</body>
</html>