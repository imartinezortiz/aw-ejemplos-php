<?php
/*
 * FUNCIONES DE APOYO
 */

function generarErrorCampoForm($idCampo, $errores = []) {
    $htmlError = null;

    if (isset($errores[$idCampo])) {
        $htmlError = "<span class=\"validation-message\">$errores[$idCampo]</span>";
    }
    return $htmlError;
}

function generaFormularioMiPagina($opciones = []) {
    $porDefecto = [
        'action' => $_SERVER['REQUEST_URI'],
        'buttonAction' => 'submit',
        'params' => [],
        'errores' => [],
    ];

    // Se utiliza el operador de [] / list() para descomponer el array $opciones
    $opts = array_merge($porDefecto, $opciones);
    [
        'action' => $action,
        'buttonAction' => $buttonAction,
        'params' => $paramReales,
        'errores' => $errores,
    ] = $opts;

    $paramsPorDefecto = [
        'nombre' => null,
        'apellido' => null,
        'edad' => null,
    ];

    $params = array_merge($paramsPorDefecto, $paramReales);

    $htmlErrores = [];
    foreach(array_keys($paramsPorDefecto) as $campo) {
        $htmlError = generarErrorCampoForm($campo, $errores);
        if ($htmlError) {
            $htmlErrores[$campo] = $htmlError;
        } else {
            $htmlErrores[$campo] = '';
        }
    }


    $attrValues = [];
    foreach( $params as $campo => $value) {
        if($value !== null) {
            $attrValues[$campo] = "value=\"$value\"";
        } else {
            $attrValues[$campo] = '';
        }
    }

    $htmlForm=<<<EOF
    <form method="POST" action="$action">
        <div>
            <label for="nombreFormUnico">Nombre: </label>
            <input type="text" id="nombreFormUnico" name="nombre" $attrValues[nombre] />$htmlErrores[nombre]
        </div>
        <div>
            <label for="apellidoFormUnico">Apellido: </label>
            <input type="text" id="apellidoFormUnico" name="apellido" $attrValues[apellido] />$htmlErrores[apellido]
        </div>
        <div>
            <label for="edadFormUnico">Edad: </label>
            <input type="number" id="edadFormUnico" name="edad" $attrValues[edad] />$htmlErrores[edad]
        </div>
        <div>
            <button type="$buttonAction" name="formAction">Enviar</button>
        </div>
    </form>
    EOF;

    return $htmlForm;
}

/*
 * LÓGICA PRINCIPAL
 */

// 1. ¿Se ha enviado el formulario o hay que mostrarlo?
$formularioEnviado = $_SERVER['REQUEST_METHOD'] === 'POST' || isset($_POST['formAction']);

$gestionForm = '';
$defaultFormOptions = $defaultFormOptions ?? ['action' => 'ejemploFormulario.php'];
if(! $formularioEnviado) {
    // 2.1 No se ha enviado, hay que mostrarlo
    $gestionForm = generaFormularioMiPagina($defaultFormOptions);
} else {
    // 2.2 Sí se ha enviado, hay que gestionarlo

    // 3. Validar los datos

    // 3.1. Validación sintáctica.
    $nombre = filter_input(INPUT_POST, 'nombre', FILTER_SANITIZE_SPECIAL_CHARS);
    $apellido = filter_input(INPUT_POST, 'apellido', FILTER_SANITIZE_SPECIAL_CHARS);
    $edad = filter_input(INPUT_POST, 'edad', FILTER_SANITIZE_NUMBER_INT);
    $params = ['nombre' => $nombre, 'apellido' => $apellido, 'edad' => $edad];

    // 3.2. Validación semántica.
    $errores = [];

    // 3.2.1 Validación del parámetro $nombre según las reglas de la aplicación
    if ( mb_strlen($nombre) < 4) {
        $errores['nombre'] = 'El nombre debe de tener al menos 4 caracteres';
    }

    // 3.2.2 Validación del parámetro $apellido según las reglas de la aplicación
    if ( mb_strlen($apellido) < 5) {
        $errores['apellido'] = 'El apellido debe de tener al menos 5 caracteres';
    }

    // 3.2.3 Validación del campo numérico
    if (intval($edad) < 12) {
        $errores['edad'] = 'Debes de tener al menos 12 años';
    }

    $esValido = count($errores) == 0;

    $formOptions = array_merge($defaultFormOptions, ['params' => $params, 'errores' => $errores]);
    // 4. Procesar los datos
    if (! $esValido) {
        // 4.1 El formulario no es válido: volver a mostrar el formulario.
        $gestionForm = generaFormularioMiPagina($formOptions);
    } else {
        // 4.2 El formulario es válido: aplicar la lógica asociada a la acción de usuario
        $gestionForm = '<p>El formulario es ha gestionado de manera correcta.</p>';
    }
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="../assets/css/estilos.css?20220304" />
  <title>Script PHP de gestión de Formularios</title>
</head>
<body>
<?= $gestionForm ?>
</body>
</html>