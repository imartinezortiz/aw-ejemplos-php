<?php
@include __DIR__.'/includes/helpers/'. pathinfo(__FILE__, PATHINFO_FILENAME) . 'Helper.php';

$origen = 'AAA-BBB-CCC-DDD';
$destino = 'XXX-YYY-ZZZ-WWW';
$cantidad = 100000000;
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../assets/css/estilos.css?20220304" />
    <title>Gestión básica de formularios</title>
</head>
<body>
    <h1>Simulación de transferencia</h1>
    <h2>Transferencia mal gestionada con <code>GET</code></h2>

    <form action="procesaTransferenciaMal.php">
        <div>
            <label for="origenM">Origen: </label>
            <input type="text" id="origenM" name="origen" value="<?= $origen ?>" />
        </div>
        <div>
            <label for="destinoM">Destino: </label>
            <input type="text" id="destinoM" name="destino" value="<?= $destino ?>" />
        </div>
        <div>
            <label for="cantidadM">Cantidad: </label>
            <input type="numeric" id="cantidadM" name="cantidad" value="<?= $cantidad ?>" />
        </div>
        <div>
            <button type="submit">Transferir</button>
        </div>
    </form>

    <h3>Misma transferencia realizada pero sin intervención del usuario</h3>
    <p style="width: 50%;margin:auto;text-align: center"><img style="max-width:256px" src="procesaTransferenciaMal.php?origen=<?= $origen ?>&destino=<?= $destino ?>&cantidad=<?= $cantidad ?>&XSS"></p>
    <!-- //: REMOVE -->
    <p>Si revisas la consola de desarrollo de Chrome verás que se ha generado una petición para transferir la cantidad 100000000€ desde la cuenta origen <?= $origen ?> a la cuenta destino <?= $destino ?>, simplemente al colar una etiqueta <code>&lt;img&gt;</code> en la página.</p>

    <p>Simplemente cambiando los <code>$_REQUEST</code> por <code>$_POST</code> en <code>procesaTransferenciaMal.php</code> el problema se soluciona. Puedes ver este <a href="transferenciaOK.php">ejemplo funcionando correctamente</a>.</p>
    <!-- //:~ -->
</body>
</html>