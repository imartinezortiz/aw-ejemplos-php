<?php
if (! isset($_REQUEST['XSS'])) {
    return;
}

function platformSlashes($path) {
    return str_replace('/', DIRECTORY_SEPARATOR, $path);
}

$httpMethod = $_SERVER['REQUEST_METHOD'];

$imagen = null;
if ($httpMethod != 'POST') {
    $imagen = platformSlashes('./../assets/img/troll.png');
} else {
    $imagen = platformSlashes('./../assets/img/yoda.png');
}

$filePath = realpath($imagen);
$tamañoImagen =  filesize($filePath);
$archivoImagen = fopen($filePath, 'rb');

header('Content-Type: image/png');
header("Content-Length: {$tamañoImagen}");
$result = fpassthru($archivoImagen);
fclose($archivoImagen);
exit();