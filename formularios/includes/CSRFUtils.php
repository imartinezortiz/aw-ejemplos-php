<?php

class CSRFUtils
{

    /**
     * @var string Sufijo para el nombre del parámetro de la sesión del usuario donde se almacena el token CSRF.
     */
    const CSRF_SESSION_PARAM = 'csrf';

    /**
     * @var string Nombre del parámetro que se se envía en el formulario que contiene el token CSRF.
     */
    const CSRF_REQUEST_PARAM = 'CSRFToken';

    /**
     * @var string Nombre del parámetro para acceder de la cabecera <code>X-XSRF-TOKEN</code> que se debe enviar
     * en una petición AJAX y cuyo valor debe ser el token CSRF almacenado en el campo del formulario cuyo nombre está
     * definido en la constante <code>self::CSRF_REQUEST_PARAM</code>.
     */
    const CSRF_AJAX_HEADER = 'HTTP_X_XSRF_TOKEN';


    /**
     * Método para eliminar los tokens CSRF almecenados en la petición anterior que no hayan sido utilizados en la actual.
     */
    public static function limpiaCsrfTokens()
    {
        foreach (array_keys($_SESSION) as $key) {
            if (strpos($key, self::CSRF_SESSION_PARAM) === 0) {
                unset($_SESSION[$key]);
            }
        }
    }

    /**
     * Genera una cadena aleatoria de 128 caracteres que se utilizará como token CSRF.
     * Este valor se almacenará en la <code>$_SESSION</code> del usuario para poder posteriormente comprobarlo.
     *
     * @param string $formId Id del formulario para que
     * @see https://www.owasp.org/index.php/PHP_CSRF_Guard Información original en la que está basada el código.
     *
     * @return string Devuelve el token CSRF a incluir en el formulario.
     *
     * @throws RuntimeException Si no es posible guardar el token en la sesión del usuario.
     */
    private static function csrfGenerateToken($formParameter)
    {
        if (!session_id()) {
            throw new \Exception('La sesión del usuario no está definida.');
        }

        $paramSession = self::CSRF_SESSION_PARAM . '_' . $formParameter;
        if (isset($_SESSION[$paramSession])) {
            $token = $_SESSION[$paramSession];
        } else {
            if (function_exists('hash_algos') && in_array('sha512', hash_algos())) {
                $token = hash('sha512', mt_rand(0, mt_getrandmax()));
            } else {
                $token = ' ';
                for ($i = 0; $i < 128; ++$i) {
                    $r = mt_rand(0, 35);
                    if ($r < 26) {
                        $c = chr(ord('a') + $r);
                    } else {
                        $c = chr(ord('0') + $r - 26);
                    }
                    $token .= $c;
                }
            }

            $_SESSION[$paramSession] = $token;
        }
        return $token;
    }

    /**
     * Valida el token CSRF recibido.
     *
     * @param string $formId Id del formulario a comprobar.
     * @param string $tokenRecibido Token CSRF recibido en la petición.
     *
     * @return boolean|string[] <code>true</code> si la validación es satisfactoria o un array que contiene una lista de
     * mensajes de error.
     *
     * @throws RuntimeException Si no es posible acceder a la sesión del usuario.
     */
    private static function csrfValidateToken($formParameter, $tokenRecibido)
    {
        if (!session_id()) {
            throw new \Exception('La sesión del usuario no está definida.');
        }

        $result = TRUE;

        $paramSession = self::CSRF_SESSION_PARAM . '_' . $formParameter;
        if (isset($_SESSION[$paramSession])) {
            if ($_SESSION[$paramSession] !== $tokenRecibido) {
                $result = array();
                $result[] = 'Has enviado el formulario dos veces';
            }
            $_SESSION[$paramSession] = ' ';
            unset($_SESSION[$paramSession]);
        } else {
            $result = array();
            $result[] = 'Formulario no válido';
        }
        return $result;
    }

    private $requestId;

    private $errores;

    public function __construct($requestId)
    {
        $this->requestId = $requestId;
        $this->errores = [];
    }

    public function generaCampoCSRF()
    {
            
        $tokenValue = self::csrfGenerateToken($this->requestId);
        $tokenCSRF = <<<EOS
        <input type="hidden" name="CSRFToken" value="$tokenValue" />
        EOS;

        return $tokenCSRF;
    }

    public function validaTokenCSRF($tokenRecibido)
    {
        $result = self::csrfValidateToken($this->requestId, $tokenRecibido);
        self::limpiaCsrfTokens();
        if ($result !== true) {
            $this->errores = $result;
            return false;
        }
        return true;
    }

    public function getErrores()
    {
        return $this->errores;
    }
}
