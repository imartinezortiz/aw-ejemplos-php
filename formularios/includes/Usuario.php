
<?php

define ('ADMIN_ROLE', 1);
define('USER_ROL', 2);

$simpleDB = [
    "user" => new Usuario("user", password_hash("userpass", PASSWORD_DEFAULT), USER_ROL),
    "admin" => new Usuario("admin", password_hash("adminpass", PASSWORD_DEFAULT), USER_ROL),
];


class Usuario {
    static function login($username, $pass) {
        // Solo para el ejemplo
        global $simpleDB;
        $user = $simpleDB[$username];
        if (! $user) return false;
        // Para comparar cadenas UTF-8 hay que usar la clase Collator, pero el password es ASCII
        if (!password_verify($pass, $user->password)) return false;
        return $user;
    } 

    private $username;

    private $password;

    private $rol;

    public function __construct($username, $password, $rol = USER_ROL) {
        $this->username = $username;
        $this->password = $password;
        $this->rol = $rol;
    }

    public function tieneRol($rol) {
        return $this->rol === $rol;
    }
}