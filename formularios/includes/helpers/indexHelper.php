<?php

/*
 * FUNCIONES DE APOYO
 */

function formularioBasico($options, $indent='  ') {
    $defaultOptions = ['visibles' => [], 'ocultos' => [], 'enviarAction' => 'submit',  'action' => 'procesaForm.php'];
    $options = array_merge($defaultOptions, $options);
    $paramVisibles = $options['visibles'];
    $paramOcultos = $options['ocultos'];
    $action = $options['action'];

    $htmlParamOcultos = '';
    foreach($paramOcultos as $key => $value) {
        $htmlParamOcultos .= <<<EOS
        <input type="hidden" name="$key" value="$value" />
        EOS;
    }
    $form = <<<EOS
    <form method="POST" action="$action">$htmlParamOcultos
    	<div>
    		<label for="nombreFormBasico">Nombre: </label>
    		<input type="text" id="nombreFormBasico" name="nombre" value="$paramVisibles[nombre]" />
    	</div>
    	<div>
    		<label for="apellidoFormBasico">Apellido: </label>
    		<input type="text" id="apellidoFormBasico" name="apellido" value="$paramVisibles[apellido]" />
    	</div>
    	<div>
    		<label for="edadFormBasico">Edad: </label>
    		<input type="number" id="edadFormBasico" name="edad" value="$paramVisibles[edad]" />
    	</div>
    	<div>
    		<button type="$options[enviarAction]">Enviar</button>
    	</div>
    </form>
    EOS;

    $output = preg_replace('/\t/mu', $indent, $form);
    return preg_replace('/\n/mu', "\n$indent", $output);
}

function ejemploFormBasico($nombre, $apellido, $edad, $action, $pathEjemplo = '', $actualizarEjemplo = false ) {

    $formularioBasico = formularioBasico(['visibles' =>['nombre' => '<?= $nombre ?>', 'apellido' => '<?= $apellido ?>', 'edad' => '<?= $edad ?>'], 'action' => $action]);
    $ejemploFormBasico = <<<EOS
    <?php
      \$nombre = '$nombre';
      \$apellido = '$apellido';
      \$edad = $edad;
    ?>
    <!DOCTYPE html>
    <html lang="es">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Formulario Básico</title>
    </head>
    <body>
      $formularioBasico
    </body>
    </html>
    EOS;

    if ($actualizarEjemplo) {
        if ($file = fopen($pathEjemplo, 'w')) {

            fwrite($file, $ejemploFormBasico);
            fclose($file);
        }
    }

    $formularioBasicoProcesado = formularioBasico([
        'visibles' => ['nombre' => $nombre, 'apellido' => $apellido, 'edad' => $edad], 
        'enviarAction' => 'button'
    ]);

    return ['codigo' => $ejemploFormBasico, 'salida' => $formularioBasicoProcesado];
}

function validacionBasica($formulario) {

    $validacionBasica = <<<'EOS'
    // 1. Validación sintáctica.
    $nombre = $_POST['nombre'] ?? null;
    $apellido = $_POST['apellido'] ?? null;
    $edad = $_POST['edad'] ?? null;

    // 2. Validación semántica.
    $esValido = true;

    // 2.1 Validación del parámetro $nombre según las reglas de la aplicación
    $esValido = $esValido && mb_strlen($nombre) >= 4;

    // 2.2 Validación del parámetro $apellido según las reglas de la aplicación
    $esValido = $esValido && mb_strlen($apellido) >= 5;

    // 2.3 Validación del campo numérico
    $esValido = $esValido && is_numeric($edad) && intval($edad) >= 12;
    EOS;

    $resultadoValidacion = <<<'EOS'
    /* 3. Implementar la lógica asociada a la petición y a los datos validados.
     * Normalmente se hace algo más interesante que mostrar un mensaje, por ejemplo, modificar la BD, etc.
     */
    $validacion = $esValido ? 'El formulario es válido' : 'El formulario NO es valido';
    EOS;

    $validacionBasicaBody = <<<EOS
      if (\$esValido) {
    ?>
      <p>Validación: <?= \$validacion ?></p>
    <?php 
      } else {
    ?>
      <p>Validación: <?= \$validacion ?></p>
      $formulario
    <?php
      }
    EOS;

    return ['validacionBasica' => $validacionBasica, 'resultadoValidacion' => $resultadoValidacion, 'validacionBasicaBody' => $validacionBasicaBody];
}

function ejemploValidacionBasica($action, $pathEjemplo = '', $actualizarEjemplo = false ) {
    
    $_POST_str = print_r($_POST, true);

    $formularioBasico = formularioBasico([
        'visibles' => ['nombre' => '<?= $nombre ?>', 'apellido' => '<?= $apellido ?>', 'edad' => '<?= $edad ?>'],
        'action' => $action,
        'enviarAction' => 'submit'
    ]);

    [   'validacionBasica' => $validacionBasica,
        'resultadoValidacion' => $resultadoValidacion,
        'validacionBasicaBody' => $validacionBasicaBody
    ] = validacionBasica($formularioBasico);

    $ejemploValidacionBasico = <<<EOS

    $validacionBasica

    $resultadoValidacion 
    ?>
    <!DOCTYPE html>
    <html lang="es">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Validación Básica</title>
    </head>
    <body>
    <?php
    $validacionBasicaBody
    ?>
    </body>
    </html>
    EOS;

    if ($actualizarEjemplo) {
        if ($file = fopen($pathEjemplo, 'w')) {
            $contenido = <<<EOS
            <?php
            $ejemploValidacionBasico
            EOS;

            fwrite($file, $contenido);
            fclose($file);
        }
    }

    $ejemploValidacionBasico = <<<EOS
    <?php
    /*
    \$_POST = $_POST_str
    */
    $ejemploValidacionBasico
    EOS;

    // Salida

    $formularioBasicoProcesado = formularioBasico([
        'visibles' => $_POST,
        'enviarAction' => 'button'
    ]);
    
    [   'validacionBasica' => $validacionBasica,
        'resultadoValidacion' => $resultadoValidacion,
        'validacionBasicaBody' => $validacionBasicaBody
    ] = validacionBasica($formularioBasicoProcesado);

    $codigo = <<<EOS
        $validacionBasica
        $resultadoValidacion
        $validacionBasicaBody
    EOS;

    return ['ejemplo' => $ejemploValidacionBasico, 'codigo' => $codigo];
}

function ejemploValidacionRegExp() {
    
    $_POST_str = print_r($_POST, true);

    $validacionRegExp = <<<'EOS'

    $origen = $_POST['origen'] ?? 'AAAA-BBBB-CCCC-DDDD';
    $dni = $_POST['dni'] ?? '1234567L';
    $mensaje = $_POST['mensaje'] ?? 'hola mundo';

    function letraDNI($numeroDNI) {
        $resto = $numeroDNI%23;  
        $letraComprobacion = 'TRWAGMYFPDXBNJZSQVHLCKE';
        return $letraComprobacion[$resto];
    }

    echo '<p>Expresiones regulares</p>';

    $salida = preg_match('/(\w{4})-(\w{1,2})-(\w{1,2})-(\w+)/', $origen, $groupMatches);

    echo "<p>Origen: $origen</p>";

    echo '<p>Salida:</p>';
    $output = print_r($salida, true);
    echo "<pre>$output</pre>";

    echo '<p>Matches:</p>';
    $output = print_r($groupMatches, true);
    echo "<pre>$output</pre>";

    $salida = preg_match('/(\d{7,8})-?(\w)/', $dni, $groupMatches);
    $dniValido = $salida === 1 && letraDni(intval($groupMatches[1])) === $groupMatches[2];
    echo "<p>DNI: $dni</p>";

    echo '<p>Salida:</p>';
    $output = print_r($salida, true);
    echo "<pre>$output</pre>";

    echo '<p>Matches:</p>';
    $output = print_r($groupMatches, true);
    echo "<pre>$output</pre>";

    echo '<p>DNI válido:</p>';
    $output = print_r($dniValido, true);
    echo "<pre>$output</pre>";

    $salida = preg_match('/(\w+)\s+(\w+)/', $mensaje, $groupMatches);

    echo '<p>Salida (unicode SIN flag):</p>';
    $output = print_r($salida, true);
    echo "<pre>$output</pre>";

    echo '<p>Matches:</p>';
    $output = print_r($groupMatches, true);
    echo "<pre>$output</pre>";

    $salida = preg_match('/(\w+)\s+(\w+)/u', $mensaje, $groupMatches);

    echo '<p>Salida (unicode CON flag):</p>';
    $output = print_r($salida, true);
    echo "<pre>$output</pre>";

    echo '<p>Matches:</p>';
    $output = print_r($groupMatches, true);
    echo "<pre>$output</pre>";
    EOS;

    $ejemploValidacionRegExp = <<<EOS
    <?php
    /*
    \$_POST = $_POST_str
    */
    $validacionRegExp
    EOS;

    return ['ejemplo' => $ejemploValidacionRegExp, 'codigo' => $validacionRegExp];
}

function ejemploValidacionFiltro() {

    $_GET_str = print_r($_GET, true);

    $validacionFiltro = <<<'EOS'

    echo '<p>Utilizando <code>filter_input()</code></p>';

    $email = filter_input(INPUT_GET, 'email', FILTER_VALIDATE_EMAIL, ['options' => ['default' => 'hola@example.com']]);
    $emailKO = filter_input(INPUT_GET, 'emailKO', FILTER_VALIDATE_EMAIL, ['options' => ['default' => 'hola@example.com']]);
    $numeroKO = filter_input(INPUT_GET, 'numero', FILTER_VALIDATE_INT);


    echo "<p>email (original): $_GET[email]</p>";
    echo "<p>email (validado): $email</p>";

    echo "<p>emailKO (original): $_GET[emailKO]</p>";
    echo "<p>emailKO (validado): $emailKO</p>";

    echo "<p>Número (original): $numeroKO";
    echo '<p>Número (validado): '. ($numeroKO === false ? 'false' : 'true' ).' </p>';

    echo '<p>Utilizando <code>filter_var()</code></p>';

    $variable = 'Pin & Pon';
    $variableSpecialChars = filter_var($variable, FILTER_SANITIZE_SPECIAL_CHARS);
    $variableSanitize = filter_var($variable, FILTER_SANITIZE_ENCODED);

    $output = htmlspecialchars($variableSpecialChars);
    echo "<p>Variable sin caracteres especiales: $output</p>";

    $output = htmlspecialchars($variableSanitize);
    echo "<p>Variable con caracteres especiales: $output</p>";

    EOS;

    $ejemploValidacionFiltro = <<<EOS
    <?php
    /*
    \$_GET = $_GET_str
    */
    $validacionFiltro
    EOS;

    return ['ejemplo' => $ejemploValidacionFiltro, 'codigo' => $validacionFiltro];

}


function ejemploFormularioGestionUnico($action, $accionExito, $urlPrefix= '../', $pathEjemplo = '', $actualizarEjemplo = false ) {
    $codigoFormularioEnScriptUnico = <<<'EOS'
    /*
     * FUNCIONES DE APOYO
     */
    
    function generarErrorCampoForm($idCampo, $errores = []) {
        $htmlError = null;
    
        if (isset($errores[$idCampo])) {
            $htmlError = "<span class=\"validation-message\">$errores[$idCampo]</span>";
        }
        return $htmlError;
    }
    
    function generaFormularioMiPagina($opciones = []) {
        $porDefecto = [
            'action' => $_SERVER['REQUEST_URI'],
            'buttonAction' => 'submit',
            'params' => [],
            'errores' => [],
        ];
    
        // Se utiliza el operador de [] / list() para descomponer el array $opciones
        $opts = array_merge($porDefecto, $opciones);
        [
            'action' => $action,
            'buttonAction' => $buttonAction,
            'params' => $paramReales,
            'errores' => $errores,
        ] = $opts;
    
        $paramsPorDefecto = [
            'nombre' => null,
            'apellido' => null,
            'edad' => null,
        ];
    
        $params = array_merge($paramsPorDefecto, $paramReales);
    
        $htmlErrores = [];
        foreach(array_keys($paramsPorDefecto) as $campo) {
            $htmlError = generarErrorCampoForm($campo, $errores);
            if ($htmlError) {
                $htmlErrores[$campo] = $htmlError;
            } else {
                $htmlErrores[$campo] = '';
            }
        }
    
    
        $attrValues = [];
        foreach( $params as $campo => $value) {
            if($value !== null) {
                $attrValues[$campo] = "value=\"$value\"";
            } else {
                $attrValues[$campo] = '';
            }
        }
    
        $htmlForm=<<<EOF
        <form method="POST" action="$action">
            <div>
                <label for="nombreFormUnico">Nombre: </label>
                <input type="text" id="nombreFormUnico" name="nombre" $attrValues[nombre] />$htmlErrores[nombre]
            </div>
            <div>
                <label for="apellidoFormUnico">Apellido: </label>
                <input type="text" id="apellidoFormUnico" name="apellido" $attrValues[apellido] />$htmlErrores[apellido]
            </div>
            <div>
                <label for="edadFormUnico">Edad: </label>
                <input type="number" id="edadFormUnico" name="edad" $attrValues[edad] />$htmlErrores[edad]
            </div>
            <div>
                <button type="$buttonAction" name="formAction">Enviar</button>
            </div>
        </form>
        EOF;
    
        return $htmlForm;
    }
    
    /*
     * LÓGICA PRINCIPAL
     */
    
    // 1. ¿Se ha enviado el formulario o hay que mostrarlo?
    $formularioEnviado = $_SERVER['REQUEST_METHOD'] === 'POST' || isset($_POST['formAction']);
    
    $gestionForm = '';

    EOS;
    $codigoFormularioEnScriptUnico .= <<<EOS
    \$defaultFormOptions = \$defaultFormOptions ?? ['action' => '$action'];

    EOS;
    $codigoFormularioEnScriptUnico .= <<<'EOS'
    if(! $formularioEnviado) {
        // 2.1 No se ha enviado, hay que mostrarlo
        $gestionForm = generaFormularioMiPagina($defaultFormOptions);
    } else {
        // 2.2 Sí se ha enviado, hay que gestionarlo
    
        // 3. Validar los datos
    
        // 3.1. Validación sintáctica.
        $nombre = filter_input(INPUT_POST, 'nombre', FILTER_SANITIZE_SPECIAL_CHARS);
        $apellido = filter_input(INPUT_POST, 'apellido', FILTER_SANITIZE_SPECIAL_CHARS);
        $edad = filter_input(INPUT_POST, 'edad', FILTER_SANITIZE_NUMBER_INT);
        $params = ['nombre' => $nombre, 'apellido' => $apellido, 'edad' => $edad];

        // 3.2. Validación semántica.
        $errores = [];
    
        // 3.2.1 Validación del parámetro $nombre según las reglas de la aplicación
        if ( mb_strlen($nombre) < 4) {
            $errores['nombre'] = 'El nombre debe de tener al menos 4 caracteres';
        }
    
        // 3.2.2 Validación del parámetro $apellido según las reglas de la aplicación
        if ( mb_strlen($apellido) < 5) {
            $errores['apellido'] = 'El apellido debe de tener al menos 5 caracteres';
        }
    
        // 3.2.3 Validación del campo numérico
        if (intval($edad) < 12) {
            $errores['edad'] = 'Debes de tener al menos 12 años';
        }
    
        $esValido = count($errores) == 0;

        $formOptions = array_merge($defaultFormOptions, ['params' => $params, 'errores' => $errores]);
        // 4. Procesar los datos
        if (! $esValido) {
            // 4.1 El formulario no es válido: volver a mostrar el formulario.
            $gestionForm = generaFormularioMiPagina($formOptions);
        } else {
            // 4.2 El formulario es válido: aplicar la lógica asociada a la acción de usuario
    
    EOS;
    $codigoFormularioEnScriptUnico .= <<<EOS
    $accionExito
        }
    }
    EOS;

    $ejemploFormularioEnScriptUnico = <<<EOS
    <?php
    $codigoFormularioEnScriptUnico
    ?>
    <!DOCTYPE html>
    <html lang="es">
    <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link rel="stylesheet" href="{$urlPrefix}assets/css/estilos.css?20220304" />
      <title>Script PHP de gestión de Formularios</title>
    </head>
    <body>
    <?= \$gestionForm ?>
    </body>
    </html>
    EOS;

    if ($actualizarEjemplo) {
        if ($file = fopen($pathEjemplo, 'w')) {
            fwrite($file, $ejemploFormularioEnScriptUnico);
            fclose($file);
        }
    }

    $codigoFormularioEnScriptUnico = <<<EOS
        \$defaultFormOptions = ['action' => 'ejemploFormulario.php', 'buttonAction' => 'button'];
        // Partimos del código del ejemplo
        $codigoFormularioEnScriptUnico
        // Imprimimos la salida
        echo \$gestionForm;
    EOS;

    return ['ejemplo' => $ejemploFormularioEnScriptUnico, 'codigo' => $codigoFormularioEnScriptUnico];
}

function ejemploRedireccionIncorrecta($pathEjemplo, $actualizarEjemplo) {
    $ejemploRedireccion = <<<'EOS'
    <html>
    <?php
    // Esta llamada falla ya para empezar e enviar HTML al cliente es necesario haber terminado previamente de enviar todas las cabeceras
    header('Location: exito.php');
    
    // Termina la ejecución del script 
    exit();
    EOS;

    if ($actualizarEjemplo) {
        if ($file = fopen($pathEjemplo, 'w')) {
            fwrite($file, $ejemploRedireccion);
            fclose($file);
        }
    }

    return $ejemploRedireccion;
}

function ejemploRedireccionCorrecta($pathEjemplo, $actualizarEjemplo) {
    $ejemploRedireccion = <<<'EOS'
    <?php
    // ...
    // Suponemos que el formulario se ha validado y la acción del usuario se ha gestionado
    $esValido = true;

    if ($esValido) {
        // Ten en cuenta que PUEDES pasar parámetros a la URL a la que redirigimos
        header('Location: exito.php?id=25&mensaje=Hola%20mundo');
        // No se sigue ejecutando la petición ya que se va a redirigir
        exit();
    }
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Contenido de la página</title>
    </head>
    <body>
        <p>Este contenido no se llega a generar ya que al llamar a <code>exit()</code>,
        la ejecución del script termina.</p>  
    </body>
    </html>
    EOS;


    if ($actualizarEjemplo) {
        if ($file = fopen($pathEjemplo, 'w')) {
            fwrite($file, $ejemploRedireccion);
            fclose($file);
        }
    }

    return $ejemploRedireccion;
}

function ejemploMultiplesParametros($action, $pathEjemplo, $actualizarEjemplo) {
    $formulario = <<<EOS
    <form action="{$action}" method="POST">
        <div>Lenguajes de programación que conoces:</div>
        <div>
            <!-- La clave es añadir [] al nombre del parámetro -->
            <label>C: <input type="checkbox" name="checkbox[]" value="c"></label>
            <label>C++: <input type="checkbox" name="checkbox[]" value="cplusplus"></label>
            <label>Java: <input type="checkbox" name="checkbox[]" value="java"></label>
            <label>PHP: <input type="checkbox" name="checkbox[]" value="php"></label>
        </div>
        <div>
            <!-- La clave es añadir [] al nombre del parámetro -->
            <select name="select[]" multiple>
            <option value="c">C</option>
            <option value="cplusplus">C++</option>
            <option value="java">Java</option>
            <option value="php">PHP</option>
            </select>
        </div>
        <div>
            <button type="submit">Enviar</button>
        </div>
    </form>
    EOS;

    $ejemploMultiplesParametros = <<<EOS
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Ejemplo múltiples valores</title>
    </head>
    <body>
        <h1>Ejemplo de formulario con parámetro y múltiples valores</h1>
        $formulario
        <p>También puedes enviar múltiples valores como parámetros de una URL como <a href="{$action}?param1[]=A&param1[]=B&param2=C">$action?param1[]=A&quot;param1[]=B&quot;param2=C</a>.</p>
        <h2>Parámetros recibidos</h2>
        <pre><code><?php print_r(\$parametrosRecibidos) ?></code></pre>
    </body>
    </html>
    EOS;

    $ejemploMultiplesParametros = <<<'EOS'
    <?php
        $formularioEnviado = strtoupper($_SERVER['REQUEST_METHOD']) === 'POST';
        if ($formularioEnviado) {
            $parametrosRecibidos = $_POST;
        } else {
            $parametrosRecibidos = $_GET;
        }
    ?>
    EOS . $ejemploMultiplesParametros;

    if ($actualizarEjemplo) {
        if ($file = fopen($pathEjemplo, 'w')) {
            fwrite($file, $ejemploMultiplesParametros);
            fclose($file);
        }
    }

    return [ 'formulario' => $formulario, 'ejemplo' => $ejemploMultiplesParametros];
}

/*
 * LOGICA PRINCIPAL
 */

// Flag para actualizar os archivos de ejemplo
$actualizarEjemplos = isset($_GET['ACTUALIZAR_EJEMPLOS']);

// 1. Ejemplo form básico

$nombre = 'Ivan';
$apellido = 'Martinez';
$edad = 18;

$pathEjemplo = implode(DIRECTORY_SEPARATOR, [dirname(dirname(__DIR__)), 'formularioBasico.php']);
[
    'codigo' => $ejemploFormBasico,
    'salida' => $formularioBasicoProcesado
] = ejemploFormBasico($nombre, $apellido, $edad, 'procesaForm.php', $pathEjemplo, $actualizarEjemplos);

// 2. Ejemplos de validación básica, datos correctos
$_POST['nombre'] = $nombre;
$_POST['apellido'] = $apellido;
$_POST['edad'] = 18;

$pathEjemplo = implode(DIRECTORY_SEPARATOR, [dirname(dirname(__DIR__)), 'procesaForm.php']);
[
    'ejemplo' => $validacionBasicaOK, 
    'codigo' => $codigoValidacionBasicaOK
] = ejemploValidacionBasica('procesaForm.php', $pathEjemplo, $actualizarEjemplos);
ob_start();
// XXX: OJO eval() es MUY peligroso (sobre todo cuando se utilizan datos que vienen del usuario) y no se debería de usar. Aquí tiene sentido para generar la salida del ejemplo
eval($codigoValidacionBasicaOK);
$salidaValidacionBasicaOK = ob_get_clean();

// 3. Ejemplos de validación básica, datos incorrectos
$_POST['nombre'] = $nombre;
$_POST['apellido'] = $apellido;
$_POST['edad'] = 1;
[
    'ejemplo' => $validacionBasicaKO, 
    'codigo' => $codigoValidacionBasicaKO
] = ejemploValidacionBasica('procesaForm.php', $pathEjemplo, $actualizarEjemplos);
ob_start();
// XXX: OJO eval() es MUY peligroso (sobre todo cuando se utilizan datos que vienen del usuario) y no se debería de usar. Aquí tiene sentido para generar la salida del ejemplo
eval($codigoValidacionBasicaKO);
$salidaValidacionBasicaKO = ob_get_clean();

// 4. Ejemplo de validación con expresiones regulares

foreach(['nombre', 'apellido', 'edad'] as $param) {
    unset($_POST[$param]);
}
$_POST['origen'] = 'ABCD-h-wx-ABCD';
$_POST['dni'] = '11223344B';
$_POST['mensaje'] = 'こんにちは 世界';
[
    'ejemplo' => $validacionRegExp, 
    'codigo' => $codigoValidacionRegExp
] = ejemploValidacionRegExp();

ob_start();
// XXX: OJO eval() es MUY peligroso (sobre todo cuando se utilizan datos que vienen del usuario) y no se debería de usar. Aquí tiene sentido para generar la salida del ejemplo
eval($codigoValidacionRegExp);
$salidaValidacionRegExp = ob_get_clean();

// 4. Ejemplo de validación con filtros

foreach(['origen', 'dni'] as $param) {
    unset($_POST[$param]);
}

[
    'ejemplo' => $validacionFiltro, 
    'codigo' => $codigoValidacionFiltro
] = ejemploValidacionFiltro();

ob_start();
// XXX: OJO eval() es MUY peligroso (sobre todo cuando se utilizan datos que vienen del usuario) y no se debería de usar. Aquí tiene sentido para generar la salida del ejemplo
eval($codigoValidacionFiltro);
$salidaValidacionFiltro = ob_get_clean();


// 5. Formulario multi-linea

$ejemploFormMultiLinea = <<<'EOS'
// 1. Validación sintáctica.
$nombre = $_POST['nombre'] ?? null;
$apellido = $_POST['apellido'] ?? null;
$edad = $_POST['edad'] ?? null;

// 2. Validación semántica.
$esValido = true;

// 2.1 Validación del parámetro $nombre según las reglas de la aplicación
$esValido = $esValido && mb_strlen($nombre) >= 4;

// 2.2 Validación del parámetro $apellido según las reglas de la aplicación
$esValido = $esValido && mb_strlen($apellido) >= 5;

// 2.3 Validación del campo numérico
$esValido = $esValido && is_numeric($edad) && intval($edad) >= 12;

/* 3. Implementar la lógica asociada a la petición y a los datos validados.
 * Normalmente se hace algo más interesante que mostrar un mensaje, por ejemplo, modificar la BD, etc.
 */
$validacion = $esValido ? 'El formulario es válido' : 'El formulario NO es valido';

$htmlForm=<<<EOF
<p>Validación: $validacion</p>
<form method="POST" action="procesaForm.php">
  <div>
    <label for="nombreFormBasico">Nombre: </label>
    <input type="text" id="nombreFormBasico" name="nombre" value="$nombre" />
  </div>
  <div>
    <label for="apellidoFormBasico">Apellido: </label>
    <input type="text" id="apellidoFormBasico" name="apellido" value="$apellido" />
  </div>
  <div>
    <label for="edadFormBasico">Edad: </label>
    <input type="number" id="edadFormBasico" name="edad" value="$edad" />
  </div>
  <div>
    <button type="submit">Enviar</button>
  </div>
</form>
EOF;
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Validación Básica</title>
</head>
<body>
<?php
if (! $esValido ) {
    echo $htmlForm;
} else {
?>
  <p>Validación: <?= $validacion ?></p>
<?php 
}
?>
</body>
</html>
EOS;

ob_start();
// XXX: OJO eval() es MUY peligroso (sobre todo cuando se utilizan datos que vienen del usuario) y no se debería de usar. Aquí tiene sentido para generar la salida del ejemplo
eval($ejemploFormMultiLinea);
$salidaEjemploFormMultiLinea = ob_get_clean();

$ejemploFormMultiLinea = "<?php\n\n$ejemploFormMultiLinea";

// 6. Generación de Formularios mediante funciones

$ejemploFormFunciones = <<<'EOS'

function generaFormulario($opciones = []) {
    // Se utiliza el operador de [] / list() para descomponer el array $opciones
    [
        'action' => $action,
        'params' => $params,
    ] = $opciones;

    $validacion = $params['validacion'] ?? null;
    $nombre = $params['nombre'] ?? null;
    $apellido = $params['apellido'] ?? null;
    $edad = $params['edad'] ?? null;

    $errores = '';
    if (! empty($validacion)) {
        $errores = "<p>Validación: $validacion</p>";
    }

    $htmlForm=<<<EOF
    <form method="POST" action="$action">
        $errores
        <div>
            <label for="nombreFormBasico">Nombre: </label>
            <input type="text" id="nombreFormBasico" name="nombre" value="$nombre" />
        </div>
        <div>
            <label for="apellidoFormBasico">Apellido: </label>
            <input type="text" id="apellidoFormBasico" name="apellido" value="$apellido" />
        </div>
        <div>
            <label for="edadFormBasico">Edad: </label>
            <input type="number" id="edadFormBasico" name="edad" value="$edad" />
        </div>
        <div>
            <button type="submit">Enviar</button>
        </div>
    </form>
    EOF;

    return $htmlForm;
}

echo generaFormulario([
    'action' => 'procesaForm.php',
    'params' => [
        'validacion' => 'Los datos NO son correctos',
        'nombre' => 'H',
        'apellido' => 'M',
        'edad' => -1
    ]
]);
EOS;

ob_start();
// XXX: OJO eval() es MUY peligroso (sobre todo cuando se utilizan datos que vienen del usuario) y no se debería de usar. Aquí tiene sentido para generar la salida del ejemplo
eval($ejemploFormFunciones);
$salidaEjemploFormFunciones = ob_get_clean();

$salidaEjemploFormFunciones = "<?php\n\n$salidaEjemploFormFunciones";

// 6. Generación y gestión de formularios en una única página
$pathEjemplo = implode(DIRECTORY_SEPARATOR, [dirname(dirname(__DIR__)), 'ejemploFormulario.php']);
$accionExito = '        $gestionForm = \'<p>El formulario es ha gestionado de manera correcta.</p>\';';
[
    'codigo' => $codigoFormularioEnScriptUnico,
    'ejemplo' => $ejemploFormularioEnScriptUnico
] = ejemploFormularioGestionUnico('ejemploFormulario.php', $accionExito, '../', $pathEjemplo, $actualizarEjemplos);

ob_start();
// XXX: OJO eval() es MUY peligroso (sobre todo cuando se utilizan datos que vienen del usuario) y no se debería de usar. Aquí tiene sentido para generar la salida del ejemplo
eval($codigoFormularioEnScriptUnico);
$salidaFormularioEnScriptUnico = ob_get_clean();

// 7. Ejemplo redirección incorrecta

$pathEjemplo = implode(DIRECTORY_SEPARATOR, [dirname(dirname(__DIR__)), 'sinBuffer', 'ejemploRedireccionIncorrecta.php']);
$ejemploRedireccionIncorrecta = ejemploRedireccionIncorrecta($pathEjemplo, $actualizarEjemplos);

// 8. Ejemplo redirección correcta

$pathEjemplo = implode(DIRECTORY_SEPARATOR, [dirname(dirname(__DIR__)), 'sinBuffer', 'ejemploRedireccionCorrecta.php']);
$ejemploRedireccionCorrecta = ejemploRedireccionCorrecta($pathEjemplo, $actualizarEjemplos);

// 9. Ejemplo de formulario con redirección

$pathEjemplo = implode(DIRECTORY_SEPARATOR, [dirname(dirname(__DIR__)), 'sinBuffer', 'ejemploFormularioRedireccion.php']);
$accionExito = <<<'EOS'
        header('Location: exito.php?mensaje=Formulario%20procesado%20correctamente');
        die();
EOS;
[
    'ejemplo' => $ejemploFormularioRedireccion
] = ejemploFormularioGestionUnico('ejemploFormularioRedireccion.php', $accionExito, '../../', $pathEjemplo, $actualizarEjemplos);


$pathEjemplo = implode(DIRECTORY_SEPARATOR, [dirname(dirname(__DIR__)), 'multiplesValores.php']);
[
    'formulario' => $formularioMultiplesParametros, 
    'ejemplo' => $ejemploMultiplesParametros
] = ejemploMultiplesParametros('multiplesValores.php', $pathEjemplo, $actualizarEjemplos);