<?php

// Flag para actualizar os archivos de ejemplo
$actualizarEjemplos = isset($_GET['ACTUALIZAR_EJEMPLOS']);

if (! $actualizarEjemplos ) {
    return;
}

$input = fopen('procesaTransferenciaMal.php', 'r');
$output = fopen('procesaTransferenciaOK.php', 'w');
if ($input !== false && $output !== false) {
    while(($line = fgets($input)) !== false) {
        $line = preg_replace('/\$_REQUEST/u', '$_POST', $line);
        fputs($output, $line);
    }
}

if ($input !== false) {
    fclose($input);
}

if ($output !== false) {
    fclose($output);
}

$input = fopen('transferencia.php', 'r');
$output = fopen('transferenciaOK.php', 'w');
$removeLine = false;
if ($input !== false && $output !== false) {
    while(($line = fgets($input)) !== false) {
        // Comienzo de sección
        $matches = preg_match('#^(?:\s*)<!--(?:\s*)//:(?:\s*)(\w+)(?:\s*)-->$#u', $line, $matchGroups);
        if ($matches === 1) {
            $command = mb_strtoupper($matchGroups[1]);
            switch($command) {
                case 'REMOVE':
                    $removeLine = true;
                    break;
            }
        }

        // Fin de sección
        $matchGroups = [];
        $matches = preg_match('#^(?:\s*)<!--(?:\s*)//:~(?:\s*)-->$#u', $line, $matches);
        if ($matches === 1) {
            $removeLine = false;
            $line = null;
        }

        if (! $removeLine && $line != null) {
            $line = preg_replace('/procesaTransferenciaMal.php/u', 'procesaTransferenciaOK.php', $line);
            fputs($output, $line);
        }
    }
}

if ($input !== false) {
    fclose($input);
}

if ($output !== false) {
    fclose($output);
}