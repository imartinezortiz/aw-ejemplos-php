<?php
// ...
// Suponemos que el formulario se ha validado y la acción del usuario se ha gestionado
$esValido = true;

if ($esValido) {
    // Ten en cuenta que PUEDES pasar parámetros a la URL a la que redirigimos
    header('Location: exito.php?id=25&mensaje=Hola%20mundo');
    // No se sigue ejecutando la petición ya que se va a redirigir
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contenido de la página</title>
</head>
<body>
    <p>Este contenido no se llega a generar ya que al llamar a <code>exit()</code>,
    la ejecución del script termina.</p>  
</body>
</html>