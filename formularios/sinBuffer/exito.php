<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Éxito</title>
</head>
<body>
    <h1>La operación se ha llevado a cabo correctamente</h1>
    <p>Han llegado los siguientes parámetros por <code>$_GET</code></p>
    <pre><samp><?php
      print_r($_GET);
    ?></samp></pre>
</body>
</html>