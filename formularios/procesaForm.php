<?php

// 1. Validación sintáctica.
$nombre = $_POST['nombre'] ?? null;
$apellido = $_POST['apellido'] ?? null;
$edad = $_POST['edad'] ?? null;

// 2. Validación semántica.
$esValido = true;

// 2.1 Validación del parámetro $nombre según las reglas de la aplicación
$esValido = $esValido && mb_strlen($nombre) >= 4;

// 2.2 Validación del parámetro $apellido según las reglas de la aplicación
$esValido = $esValido && mb_strlen($apellido) >= 5;

// 2.3 Validación del campo numérico
$esValido = $esValido && is_numeric($edad) && intval($edad) >= 12;

/* 3. Implementar la lógica asociada a la petición y a los datos validados.
 * Normalmente se hace algo más interesante que mostrar un mensaje, por ejemplo, modificar la BD, etc.
 */
$validacion = $esValido ? 'El formulario es válido' : 'El formulario NO es valido'; 
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Validación Básica</title>
</head>
<body>
<?php
  if ($esValido) {
?>
  <p>Validación: <?= $validacion ?></p>
<?php 
  } else {
?>
  <p>Validación: <?= $validacion ?></p>
  <form method="POST" action="procesaForm.php">
    <div>
      <label for="nombreFormBasico">Nombre: </label>
      <input type="text" id="nombreFormBasico" name="nombre" value="<?= $nombre ?>" />
    </div>
    <div>
      <label for="apellidoFormBasico">Apellido: </label>
      <input type="text" id="apellidoFormBasico" name="apellido" value="<?= $apellido ?>" />
    </div>
    <div>
      <label for="edadFormBasico">Edad: </label>
      <input type="number" id="edadFormBasico" name="edad" value="<?= $edad ?>" />
    </div>
    <div>
      <button type="submit">Enviar</button>
    </div>
  </form>
<?php
  }
?>
</body>
</html>