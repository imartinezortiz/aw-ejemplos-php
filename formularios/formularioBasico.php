<?php
  $nombre = 'Ivan';
  $apellido = 'Martinez';
  $edad = 18;
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Formulario Básico</title>
</head>
<body>
  <form method="POST" action="procesaForm.php">
    <div>
      <label for="nombreFormBasico">Nombre: </label>
      <input type="text" id="nombreFormBasico" name="nombre" value="<?= $nombre ?>" />
    </div>
    <div>
      <label for="apellidoFormBasico">Apellido: </label>
      <input type="text" id="apellidoFormBasico" name="apellido" value="<?= $apellido ?>" />
    </div>
    <div>
      <label for="edadFormBasico">Edad: </label>
      <input type="number" id="edadFormBasico" name="edad" value="<?= $edad ?>" />
    </div>
    <div>
      <button type="submit">Enviar</button>
    </div>
  </form>
</body>
</html>