<?php

// 1. Validación sintática.

$origen = $_REQUEST['origen'] ?? null;
$destino = $_REQUEST['destino'] ?? null;
$cantidad = $_REQUEST['cantidad'] ?? null;

// 2. Validación semántica.
$esValido = true;

// 2.1 Validación de la cuenta origen
$esValido = $esValido && preg_match('/(\w+)-(\w+)-(\w+)-(\w+)/u', $origen);

// 2.2 Validación de la cuenta destino
$esValido = $esValido && preg_match('/(\w+)-(\w+)-(\w+)-(\w+)/u', $destino);

// 2.3 Validación de la cantidad
$esValido = $esValido && preg_match('/^(\d+)(?:,|.)(\d{0,2})$/u', $cantidad, $cantidadMatch)  && floatval("{$cantidadMatch[1]}.{$cantidadMatch[2]}") >= 0.0 ;

// 3. Resultado final de la validación
$validacion = $esValido ? 'El formulario es válido' : 'El formulario NO es valido';

// BEGIN_EJEMPLO: ESTE CÓDIGO SÓLO SE INCLUYE PARA FACILITAR LA EXPLICACIÓN / EJEMPLO, NO SE DEBERÍA DE INCLUIR EN UNA APLICACIÓN REAL
if ( $esValido ) {
    require __DIR__ .'/includes/demo/transferenciaRogue.php';
}
// END_EJEMPLO
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Procesado transferencia</title>
</head>
<body>
    <h1>Procesado transferencia</h1>
<?php if ( $esValido ) { ?>
    <h2>Transferencia emitida</h2>
    <p>Detalles de la transferencia:</p>
    <ul>
        <li>Origen: <?= $origen ?></li>
        <li>Destino: <?= $destino ?></li>
        <li>Cantidad: <?= $cantidad ?></li>
    </ul>
<?php } else { ?>
    <p><?= $validacion ?></p>
    <form action="procesaTransferenciaMal.php">
        <div>
            <label for="origenM">Origen: </label>
            <input type="text" id="origenM" name="origen" value="<?= $origen ?>" />
        </div>
        <div>
            <label for="destinoM">Destino: </label>
            <input type="text" id="destinoM" name="destino" value="<?= $destino ?>" />
        </div>
        <div>
            <label for="cantidadM">Cantidad: </label>
            <input type="numeric" id="cantidadM" name="cantidad" value="<?= $cantidad ?>" />
        </div>
        <div>
            <button type="submit">Transferir</button>
        </div>
    </form>
<?php } ?>
</body>
</html>