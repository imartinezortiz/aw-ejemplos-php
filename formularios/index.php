<?php
@include __DIR__.'/includes/helpers/'. pathinfo(__FILE__, PATHINFO_FILENAME) . 'Helper.php';
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/estilos.css?20220304" />
    <script src="../assets/js/main.js"></script>
    <title>Gestión básica de formularios</title>
</head>
<body>
    <h1>Gestión básica de formularios</h1>

    <p>Los formularios son un tipo concreto de lógica de <a href="../peticiones/index.php">gestión de peticiones en PHP</a>, por lo que si todavía no has pasado por esa página, te recomiendo que le eches un vistazo.</p>

    <nav>
        <p>Contenidos de la página</p>
        <ul>
            <li><a href="#recordatorio">Recordatorios acerca del HTML de los formularios</a>.</li>
            <li><a href="#validacion">Validación de los parámetros de un formulario</a>.</li>
            <ul>
                <li><a href="#primeros-pasos">Primeros pasos en la validación</a></li>
                <li><a href="#mejorando-validacion">Mejorando la validación de los parámetros</a></li>
                <li><a href="#simplificar-redundancias">Simplificando redundancias</a></li>
            </ul>
            <li><a href="#mejora-usabilidad">Mejora de la usabilidad: evitar mensajes molestos e innecesarios</a></li>
            <li><a href="#multiples-valores">¿Cómo procesar un parámetro que tiene múltiples valores (<code>&lt;select&gt;</code>, <code>&lt;input type=&quot;checkbox&quot; /&gt;</code>) ?</a></li>
        </ul>
    </nav>

    <h2 id="recordatorio">Recordatorios acerca del HTML de los formularios</h2>
    <p>Para facilitar la creación y gestión de los formularios, debes de recordar que:
    <ul>
        <li>Debes definir los atributos <code>name</code> en los campos de los formularios para que lleguen al servidor.</li>

        <li>Es (más que) recomendable que utilices la etiqueta <code><a href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/label" target="_blank">&lt;label&gt;</a></code> para el texto asociado a la descripción de los campos del formulario. En particular, recuerda la relación entro los atributos <code>&lt;label for=&quot;...&quot;</code> y <code>&lt;input id=&quot;...&quot;</code> (ó <code>&lt;select id=</code> ó <code>&lt;input id=&quot;...&quot;</code>).</li>

        <li>Tienes disponibles una gran <a href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#input_types">variedad de campos de formulario</a> que te permiten simplificar y mejorar la experiencia del usuario.</li>

        <li>La validación en cliente (al utilizar atributos HTML o javascript) se utiliza para mejorar la experiencia de usuario, <strong>SIEMPRE debes de validar en el servidor</strong>.</li>

        <li>Todos los <strong>formularios que modifiquen el estado del servidor de algún modo deben deben gestionarse con HTTP POST</strong>. Recuerda que puedes utilizar la <a href="https://developer.chrome.com/docs/devtools/">consola de desarrollo</a> para verificar el tipo de petición y el contenido en bruto tanto de la petición como de la respuesta asociada a la gestión de un formulario (pestaña Network).</li>
    </ul>

    <aside class="advertencia">Tanto por motivos de gestión como por motivos de seguridad normalmente deberás de añadir <code>method=&quot;POST&quot;</code> a tus formularios.</aside>

    <section class="ejemplo">
        <h6>Ejemplo formulario básico (<a href="formularioBasico.php">formularioBasico.php</a>)</h6>
        <pre><code><?= htmlspecialchars($ejemploFormBasico, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
        <p id="form-basico">Se mostraría del siguiente modo:</p>
        <div class="salida"><?= $formularioBasicoProcesado ?></div>
    </section>

    <h2 id="validacion">Validación de los parámetros de un formulario</h2>

    <p>Cualquier dato que venga del usuario debe de ser utilizado con recelo, en particular, se puede o bien <em>validar e informar</em> al usuario si el dato no tiene el tipo o el formato esperado, o bien <em>higienizar</em> el parámetro eliminando el los caracteres que no se admitan o proporcionado un valor por defecto.</p>

    <h3 id="primeros-pasos">Primeros pasos en la validación</h3>

    <p>La lógica de gestión en los scripts PHP, en particular en los encargados de gestionar formularios, debe de ejecutarse antes de generar cualquier tipo de salida para el usuario. Además esta validación se suele dividir en varias etapas:</p>
    <ol>
        <li><strong>Validación sintáctica de los parámetros</strong>. Verificamos que los datos tienen la "pinta" (e.g. estructura, tipos, longitud, etc.) que esperamos.</li>
        <li><strong>Validación semántica de los parámetros</strong>. Verificamos que los datos tienen sentido desde el punto de vista de las reglas de negocio de la aplicación. Por ejemplo:</li>
        <ul>
            <li>Verificar que los datos están dentro de los rangos admitidos.</li>
            <li>Verificar que un dato concreto existe en la base de datos.</li>
            <li>Verificar que <em>tienes permisos</em> para modificar o realizar la operación sobre esos datos concretos.</li>
        </ul>
    </ol>

    <aside class="advertencia">Recuerda utilizar la variable <code>$_POST</code> para asegurarte de que sólo gestionas el envío del formulario cuando se envía por petición POST. Si lo olvidas, pueden darse comportamientos no esperados como en este <a href="transferencia.php">ejemplo de transferencia bancaria</a>.</aside>

    <aside class="consejo">El operador <a href="../peticiones/index.php#ejemplo-nullcoalescing">??</a> te puede ser de gran utilidad para gestionar los parámetros asociados a los formularios.</aside>

    <p>Teniendo en cuenta estos aspectos, un primer intento para gestionar la lógica del formulario sería:</p>

    <section class="ejemplo">
        <h6>Validación básica del formulario, datos válidos (<a href="procesaForm.phps">fuente procesaForm.php</a>)</h6>
        <pre><code><?= htmlspecialchars($validacionBasicaOK, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
        <p>Se mostraría del siguiente modo:</p>
        <div class="salida"><?= $salidaValidacionBasicaOK ?></div>
    </section>

    <p>Si los datos son incorrectos la salida sería:</p>

    <section class="ejemplo">
        <h6>Validación básica del formulario, datos no válidos (<a href="procesaForm.phps">fuente procesaForm.php</a>)</h6>
        <pre><code><?= htmlspecialchars($validacionBasicaKO, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
        <p>Se mostraría del siguiente modo:</p>
        <div class="salida"><?= $salidaValidacionBasicaKO ?></div>
    </section>

    <h3 id="mejorando-validacion">Mejorando la validación de los parámetros</h3>

    <p>Normalmente la <em>validación sintáctica</em> tiene que mejorarse ya sea o bien porque los datos que solicitamos al usuario no son tan simples, por ejemplo:</p>
    <ul>
        <li>Una dirección de correo electrónico: imartinez@example.com</li>
        <li>Un NIF que tiene 6-7 dígitos y una letra (que es un <a href="http://www.interior.gob.es/web/servicios-al-ciudadano/dni/calculo-del-digito-de-control-del-nif-nie">código de redundancia para verificar que DNI es valido</a>), quizás con un guión de separación: 1234567L, 01234567L, 01234567-L</li>
        <li>CIFs y NIE que también tienen una estructura concreta</li>
    </ul>
    <p>En estos casos necesitamos lógica más potente para realizar las validaciones. En PHP tenemos disponibles un conjunto avanzado de funciones para tratar cadenas y poder facilitar la validación. Por ejemplo, podemos utilizar:</p>
    <ul>
        <li>Utilizar <code>htmlspecialchars(trim(strip_tags($variable)))</code> para eliminar caracteres especiales que puedan darnos problemas.</li>
        <li><a href="https://en.wikipedia.org/wiki/Regular_expression">Expresiones regulares</a> y el módulo de <a href="https://www.php.net/manual/en/book.pcre.php">expresiones regulares de PHP</a>. Ten en cuenta que cuando trabajamos con la codificación UTF-8, tenemos que recordar el añadir el modificador Puedes aprender expresiones regulares en las siguientes páginas:</li>
        <ul>
            <li><a href="https://www.regular-expressions.info/php.html">Using regular expressions with PHP</a></li>
            <li><a href="https://regexlearn.com/learn">RegexLearn</a></li>
            <li><a href="https://www.regular-expressions.info/unicode.html">Unicode Regular Expressions</a>. Muy útil cuando quieres procesar texto UTF-8 y dar un soporte apropiado.</li>
        </ul>
        <li><a href="https://www.php.net/manual/en/book.filter.php">El módulo de filtros</a>. Este módulo define un conjunto de funciones bastante potente que son especialmente útiles para validar las variables superglobales con <a href="https://www.php.net/manual/en/function.filter-input.php"><code>filter_input()</code></a>, pero también son aplicables a cualquier otra variable con <a href="https://www.php.net/manual/en/function.filter-var.php"><code>filter_var()</code></a>. Este módulo contiene filtros para <em>validar</em> la entrada, es decir, comprobar que la entrada cumple ciertos criterio (e.g. FILTER_VALIDATE_EMAIL valida que un parámetro coincide con la estructura de un email válido), o <em>higieniza</em> la entrada, es decir, elimina los caracteres que no son válidos para el filtro configurado.</li>
    </ul>

    <aside class="consejo">Cuando utilizamos el <a href="https://www.php.net/manual/en/book.pcre.php">módulo PCRE de PHP</a>, tenemos que acordarnos de utilizar el modificador <a href="https://www.php.net/manual/en/reference.pcre.pattern.modifiers.php"><em>u</em></a> en las expresiones regulares para procesar adecuadamente los caracteres codificados en UTF-8 y también podemos utilizar <a href="https://www.php.net/manual/en/regexp.reference.unicode.php">secuencias de escape unicode</a></aside>

    <section class="ejemplo">
        <h6>Validación con expresiones regulares</h6>
        <pre><code><?= htmlspecialchars($validacionRegExp, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
        <p>Generan la siguiente salida:</p>
        <div class="salida"><?= $salidaValidacionRegExp ?></div>
    </section>

    <p> Para que funcione adecuadamente el siguiente ejemplo, tienes que pulsar sobre el enlace &quot;probar ejemplo&quot;</p>
 
    <section id="ejemplo-filtro" class="ejemplo">
        <h6>Validación con filtros (<a href="<?= $_SERVER['PHP_SELF'] ?>?email=imartinez@example.com&numero=11223344B#ejemplo-filtro">probar ejemplo</a>)</h6>
        <pre><code><?= htmlspecialchars($validacionFiltro, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
        <p>Generan la siguiente salida:</p>
        <div class="salida"><?= $salidaValidacionFiltro ?></div>
    </section>

    <h3 id="simplificar-redundancias">Simplificando redundancias</h3>

    <p>Como puedes ver en los ejemplos anteriores, hay bastante código repetido entre las páginas <code>formularioBasico.php</code> y <code>procesaForm.php</code>, en especial es código relativo a la generación de los formularios.</p>
    <p>Además, en <code>procesaForm.php</code> el código es todavía más complejo, ya que es necesario distinguir si el formulario ha podido ser procesado exitosamente (los datos son válidos y la acción asociada al mismo ha podido ejecutarse in problemas) o bien si ha sucedido algún tipo de problema, es necesario, es necesario volver a mostrar el formulario</p>

    <p>Para resolver este problema tenemos varias opciones:</p>

    <ol>
        <li><em>Utilizar cadenas multi-línea generar los bloques de HTML</em>. Esta solución resuelve el parcialmente problema ya que nos permite organizar el código de manera más adecuada.</li>
        <li><em>Utilizar funciones de apoyo para generar el HTML asociado al formulario</em>. Esta solución, además de facilitar la organización del código, también nos permite eliminar las redundancias entre páginas (e.g. en <code>formularioBasico.php</code> y <code>procesaForm.php</code>). Además, esta solución habitualmente se apoyará en el uso de cadenas multi-línea en su implementación. Asimismo, esta opción implementada con un poco de cuidado se puede utilizar para poder generar formularios de manera genérica.</li>
    </ol>

    <section class="ejemplo">
        <h6>Ejemplo de uso de cadenas multi-línea para simplificar el código</h6>
        <pre><code><?= htmlspecialchars($ejemploFormMultiLinea, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
        <p>Quedando del siguiente modo:</p>
        <div class="salida"><?= $salidaEjemploFormMultiLinea ?></div>
    </section>

    <section class="ejemplo">
        <h6>Ejemplo de funciones para generar el HTML de los formularios</h6>
        <pre><code><?= htmlspecialchars($ejemploFormFunciones, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
        <p>Quedando del siguiente modo:</p>
        <div class="salida"><?= $salidaEjemploFormFunciones ?></div>
    </section>

    <p>Otra mejora que podemos utilizar es fusionar las dos páginas que hemos venido utilizando <code>formA.php</code> y <code>procesaFormA.php</code> en una página, haciendo distinción de casos para simplificar el código:</p>

    <section class="ejemplo">
        <h6>Ejemplo de script PHP que muestra y gestiona un fomulario (<a href="ejemploFormulario.php">ejemploFormulario.php</a>)</h6>
        <pre><code><?= htmlspecialchars($ejemploFormularioEnScriptUnico, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>

    <h2 id="mejora-usabilidad">Mejora de la usabilidad: evitar mensajes molestos e innecesarios</h2>

    <p>En todos los ejemplos anteriores, cuando se procesa una petición de manera exitosa, simplemente se modifica el cuerpo de la página para mostrar un mensaje de éxito. Normalmente, como resultado de procesar los datos de un formulario de manera correcta <strong>implementaremos un comportamiento más elaborado</strong>. Por ejemplo, en el caso de un formulario de alta de un producto en una tienda online, lo normal es que como resultado de ese proceso de alta no sólo se informe del éxito de la operación sino que, además, también se muestre el producto como lo verían los clientes a través del portal.</p>

    <p>Además de este comportamiento más elaborado, en todos los ejemplos anteriores como resultado de generar la respuesta <strong>en la misma petición HTTP</strong>, tenemos una serie de efectos no deseados. Estos efectos son visibles, cuando el usuario recarga la página o, en algunas ocasiones, si utiliza los botones de navegación (página previa y página siguiente). En ambos casos, el navegador mostrará unos mensajes de advertencia.</p>

    <figure id="reenvioWarning">
        <img class="w-50" src="assets/img/reenvioFormulario.png" alt="Captura de pantalla que muestra una advertencia de reenvío de formulario" >
        <figcaption>Advertencia de reenvío de formulario al recargar la página</figcaption>
    </figure>
    <figure id="">
        <img class="w-50" src="assets/img/err_cache_miss_Form.png" alt="Captura de pantalla que muestra una advertencia de reenvío de formulario">
        <figcaption>Advertencia de reenvío de formulario al pulsar el botón atrás si un script PHP se encarga de mostrar y gestionar un formulario.</figcaption>
    </figure>

    <p>Para evitar estos problemas y, por tanto, mejorar la usabilidad de nuestro sitio web, tenemos que cambiar el comportamiento en nuestra gestión de formularios, de modo que la acción resultante de un procesamiento exitoso del formulario <strong>se realize en una petición HTTP diferente</strong>.</p>

    <p>Para lograr este comportamiento, tenemos que manipular <em>las cabeceras de la respuesta del servidor</em>, en particular tenemos que generar una cabecera <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Location">Location</a> y, además, modificar el código de respuesta de la petición (e.g. cambiarlo de 200 a un código 30X). Para llevar a cabo esta tarea podemos utilizar la función PHP <a href="https://www.php.net/manual/en/function.header"><code>header()</code></a>.</p>

    <p><strong>Recuerda que en el protocolo HTTP</strong> las cabeceras siempre viajan al comienzo, en particular en una <a href="#respuesta-HTTP">respuesta HTTP</a>, las cabeceras viajan antes que el <em>cuerpo</em> de la petición que es donde Apache+PHP incluyen el HTML que generas.</p>

    <section id="ejemplo-http" class="ejemplo">
        <h6>Ejemplo de respuesta HTTP 1.1 (<a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview">fuente MDN</a>)</h6>
        <pre><samp>
GET / HTTP/1.1
Host: developer.mozilla.org
Accept-Language: fr
        </samp></pre>
    </section>

    <section id="respuesta-HTTP" class="ejemplo">
        <h6>Ejemplo de respuesta HTTP 1.1 (<a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview">fuente MDN</a>)</h6>
        <pre><samp>
HTTP/1.1 200 OK
Date: Sat, 09 Oct 2010 14:28:02 GMT
Server: Apache
Last-Modified: Tue, 01 Dec 2009 20:18:22 GMT
ETag: "51142bc1-7449-479b075b2891b"
Accept-Ranges: bytes
Content-Length: 29769
Content-Type: text/html

&lt;!DOCTYPE html... (here come the 29769 bytes of the requested web page)
        </samp></pre>
    </section>

    <p>Además, en el caso concreto de las redirecciones HTTP mediante el uso de los códigos de respuesta 300, el cuerpo de la respuesta <em>debe estar vacía</em>. En la siguiente respuesta puede verse la cabecera <code>Content-Length</code>. En el caso de que llegara algún contenido como parte del cuerpo, el navegador lo ignoraría, pero el problema es que <strong>estamos gastando recursos del servidor</strong> de manera innecesaria.</p>

    <section id="respuesta-HTTP" class="ejemplo">
        <h6>Respuesta HTTP para una redirección HTTP</h6>
        <pre><samp>
HTTP/1.1 302 Found
Date: Thu, 24 Feb 2022 16:13:42 GMT
Server: Apache/2.4.48 (Win64) OpenSSL/1.1.1k PHP/8.0.7
X-Powered-By: PHP/8.0.7
Location: exito.php?id=25
<mark>Content-Length: 0</mark>
Keep-Alive: timeout=5, max=96
Connection: Keep-Alive
Content-Type: text/html; charset=UTF-8
        </samp></pre>
    </section>

    <p>Teniendo en cuenta estas consideraciones podemos </p>

    <p>Para evitar estos problemas es necesario <em>redirigir</em> al usuario (forzar al navegador a realizar una nueva petición), utilizando la función <a href="https://www.php.net/manual/en/function.header">header()</a></p>

    <section class="ejemplo">
        <h6>Ejemplo de uso de header() incorrecto (<a href="sinBuffer/ejemploRedireccionIncorrecta.php">ejemploRedireccionIncorrecta.php</a>)</h6>
        <pre><code><?= htmlspecialchars($ejemploRedireccionIncorrecta, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>

    <section class="ejemplo">
        <h6>Ejemplo de uso de header() correct (<a href="sinBuffer/ejemploRedireccionCorrecta.php">ejemploRedireccionCorrecta.php</a>)</h6>
        <pre><code><?= htmlspecialchars($ejemploRedireccionCorrecta, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>

    <p>Una vez que ya sabemos utilizar la función <code>header()</code> podemos aplicarla a la gestión de formularios.</p>

    <section class="ejemplo">
        <h6>Ejemplo de Formulario con redirección (<a href="sinBuffer/ejemploFormularioRedireccion.php">ejemploFormularioRedireccion.php</a>)</h6>
        <pre><code><?= htmlspecialchars($ejemploFormularioRedireccion, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>

    <h2 id="multiples-valores">Parámetros de fomularios con múltiples valores</h2>
    
    <p>Cuando queremos que un parámetro de un formulario pueda tomar múltiples valores tenemos que añadir el sufijo <code>[]</code> al nombre del parámetro, por ejemplo, <code>&lt;select name=&quot;aficiones[]&quot;&gt;</code>.</p>

    <p>Nótese que también es posible que cuando construimos una URL también es posible enviar varios parámetros con el mismo nombre del mismo modo <code>multiplesValores.php?param1[]=A&quot;param1[]=B&quot;param2=C</code></p>

    <aside class="advertencia">Si olvidamos incluir <code>[]</code> en el nombre de los parámetros sólo podremos acceder al primer valor asociado al parámetro.</aside>
    
    <section class="ejemplo">
        <h6>Ejemplo de formulario con parámetros con múltiples valores (<a href="multiplesValores.php">multiplesValores.php</a>)</h6>
        <pre><code><?= htmlspecialchars($formularioMultiplesParametros, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>
</body>
</html>
