<?php
    $formularioEnviado = strtoupper($_SERVER['REQUEST_METHOD']) === 'POST';
    if ($formularioEnviado) {
        $parametrosRecibidos = $_POST;
    } else {
        $parametrosRecibidos = $_GET;
    }
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejemplo múltiples valores</title>
</head>
<body>
    <h1>Ejemplo de formulario con parámetro y múltiples valores</h1>
    <form action="multiplesValores.php" method="POST">
    <div>Lenguajes de programación que conoces:</div>
    <div>
        <!-- La clave es añadir [] al nombre del parámetro -->
        <label>C: <input type="checkbox" name="checkbox[]" value="c"></label>
        <label>C++: <input type="checkbox" name="checkbox[]" value="cplusplus"></label>
        <label>Java: <input type="checkbox" name="checkbox[]" value="java"></label>
        <label>PHP: <input type="checkbox" name="checkbox[]" value="php"></label>
    </div>
    <div>
        <!-- La clave es añadir [] al nombre del parámetro -->
        <select name="select[]" multiple>
        <option value="c">C</option>
        <option value="cplusplus">C++</option>
        <option value="java">Java</option>
        <option value="php">PHP</option>
        </select>
    </div>
    <div>
        <button type="submit">Enviar</button>
    </div>
</form>
    <p>También puedes enviar múltiples valores como parámetros de una URL como <a href="multiplesValores.php?param1[]=A&param1[]=B&param2=C">multiplesValores.php?param1[]=A&quot;param1[]=B&quot;param2=C</a>.</p>
    <h2>Parámetros recibidos</h2>
    <pre><code><?php print_r($parametrosRecibidos) ?></code></pre>
</body>
</html>