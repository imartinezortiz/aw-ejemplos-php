<?php
require_once __DIR__.'/includes/config.php';

$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
$privilegio = $_SESSION['privilegio'] ?? Imagen::PUBLICA;
$imagen = Imagen::buscaPorId($id);


// Comprobamos si tenemos privilegios suficientes
if ($privilegio != Imagen::PRIVADA) {
    http_response_code(403);
    exit();
}

// Comprobar si la imagen existe
if (!$imagen) {
    http_response_code(404);
    exit();
}

$rutaImg = implode(DIRECTORY_SEPARATOR, [RUTA_ALMACEN_PRIVADO, $imagen->ruta]);
$tamañoImagen =  filesize($rutaImg);
$archivoImagen = fopen($rutaImg, 'rb');

header("Content-Type: {$imagen->mimeType}");
header("Content-Length: {$tamañoImagen}");
fpassthru($archivoImagen);
fclose($archivoImagen);
