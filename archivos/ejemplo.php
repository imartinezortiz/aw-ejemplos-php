<?php
require_once __DIR__.'/includes/config.php';


$formPrivilegios = new FormularioPrivilegios();
$htmlFormPrivilegios = $formPrivilegios->gestiona();

$formUpload = new FormularioUpload();
$htmlFormUpload = $formUpload->gestiona();

$imagenesPublicas = Imagen::buscaPorTipoAcceso(Imagen::PUBLICA);
$imagenesPrivadas = Imagen::buscaPorTipoAcceso(Imagen::PRIVADA);
RUTA_ALMACEN_PUBLICO
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<title>Ejemplo de formulario con archivos</title>
    <script type="text/javascript" src="utils.js"></script>
	</head>
  <body>
    <h2>Subir nueva imagen</h2>
    <?= $htmlFormUpload ?>
    <h3>Cambiar privilegios</h3>
    <p>Si tienes el privilegio &quot;Privado&quot; puedes acceder a todas las imágenes.</p>
    <?= $htmlFormPrivilegios ?>
    <h2>Imagenes Públicas <?= count($imagenesPublicas)?> </h2>
<?php foreach($imagenesPublicas as $imagen): ?>
    <div><img src="almacenPublico/<?= $imagen->ruta?>"></div>
<?php endforeach ?>
    <h2>Imagenes Privadas</h2>
<?php foreach($imagenesPrivadas as $imagen): ?>
  <div><img src="imagen.php?id=<?= $imagen->id ?>"></div>
<?php endforeach ?>
  </body>
</html>