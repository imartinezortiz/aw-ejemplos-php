<?php

/**
 * Parámetros de conexión a la BD
 */
define('BD_HOST', 'localhost');
define('BD_NAME', 'imagenes');
define('BD_USER', 'imagenes');
define('BD_PASS', 'imagenes');

/**
 * Parámetros de la aplicación
 */
define('RUTA_ALMACEN_PUBLICO', implode(DIRECTORY_SEPARATOR, [dirname(__DIR__), 'almacenPublico']));
define('RUTA_ALMACEN_PRIVADO', implode(DIRECTORY_SEPARATOR, [__DIR__, 'almacenPrivado']));

require_once __DIR__.'/../../clases/Formulario.php';
require_once __DIR__.'/../../clases/BD.php';
require_once __DIR__.'/../../clases/MagicProperties.php';
require_once __DIR__.'/Imagen.php';
require_once __DIR__.'/FormularioUpload.php';
require_once __DIR__.'/FormularioPrivilegios.php';

session_start([
    'name' => 'EJEMPLO_ARCHIVOS'
]);