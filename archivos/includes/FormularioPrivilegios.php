<?php

class FormularioPrivilegios extends Formulario
{

    const PRIVILEGIOS = [Imagen::PUBLICA => 'Publico', Imagen::PRIVADA =>'Privado'];

    public function __construct()
    {
        parent::__construct('privilegios', ['urlRedireccion' => 'ejemplo.php']);
    }

    protected function generaCamposFormulario(&$datos)
    {
        // Se generan los mensajes de error si existen.
        $htmlErroresGlobales = self::generaListaErroresGlobales($this->errores);
        $erroresCampos = self::generaErroresCampos(['privilegio'], $this->errores, 'span', array('class' => 'error'));

        $privilegioActual = $_SESSION['privilegio'] ?? Imagen::PUBLICA;

        $selectorPrivilegios = self::generaSelectorPrivilegios('privilegio', $privilegioActual, 'privilegio');
        $html = <<<EOS
        $htmlErroresGlobales
            <div><label for="tipo">Privilegio: $selectorPrivilegios</label>{$erroresCampos['privilegio']}<button type="submit">Cambiar</button></div>            
        EOS;

        return $html;
    }

    private static function generaSelectorPrivilegios($name, $privilegioActual=null, $id=null)
    {
        $id= $id !== null ? "id=\"{$id}\"" : '';
        $html = "<select {$id} name=\"{$name}\">";
        foreach(self::PRIVILEGIOS as $clave => $valor) {
            $selected='';
            if ($privilegioActual && $clave == $privilegioActual) {
                $selected='selected';
            }
            $html .= "<option value=\"{$clave}\"{$selected}>{$valor}</option>";
        }
        $html .= '</select>';

        return $html;
    }

    protected function procesaFormulario(&$datos)
    {
        $this->errores = [];

        $privilegio = $datos['privilegio'] ?? Imagen::PUBLICA;

        $ok = array_key_exists($privilegio, self::PRIVILEGIOS);
        if (!$ok) {
            $this->errores['privilegio'] = 'El tipo del privilegio no está seleccionado o no es correcto';
        }

        if (count($this->errores) > 0) {
            return;
        }

        $_SESSION['privilegio'] = $privilegio;
    }
}
