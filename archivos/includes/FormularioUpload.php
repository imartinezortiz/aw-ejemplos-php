<?php

class FormularioUpload extends Formulario
{

    const EXTENSIONES_PERMITIDAS = array('gif', 'jpg', 'jpe', 'jpeg', 'png', 'webp', 'avif');

    const TIPO_ALMACEN = [Imagen::PUBLICA => 'Publico', Imagen::PRIVADA =>'Privado'];

    public function __construct()
    {
        parent::__construct('subir', ['enctype' => 'multipart/form-data', 'urlRedireccion' => 'ejemplo.php']);
    }

    protected function generaCamposFormulario(&$datos)
    {
        // Se generan los mensajes de error si existen.
        $htmlErroresGlobales = self::generaListaErroresGlobales($this->errores);
        $erroresCampos = self::generaErroresCampos(['archivo', 'tipo'], $this->errores, 'span', array('class' => 'error'));

        $tipoAlmacenSeleccionado = $datos['tipo'] ?? null;
        $selectorTipoAlmacen = self::generaSelectorTipoAlmacen('tipo', $tipoAlmacenSeleccionado, 'tipo');
        $html = <<<EOS
        $htmlErroresGlobales
        <fieldset>
            <legend>Subida de archivo</legend>
            <div><label for="tipo">Tipo almacén: $selectorTipoAlmacen</label>{$erroresCampos['tipo']}</div>
            <div><label for="archivo">Archivo: <input type="file" name="archivo" id="archivo" /></label>{$erroresCampos['archivo']}</div>
            <button type="submit">Subir</button>
        </fieldset>
        EOS;

        return $html;
    }

    private static function generaSelectorTipoAlmacen($name, $tipoSeleccionado=null, $id=null)
    {
        $id= $id !== null ? "id=\"{$id}\"" : '';
        $html = "<select {$id} name=\"{$name}\">";
        foreach(self::TIPO_ALMACEN as $clave => $valor) {
            $selected='';
            if ($tipoSeleccionado && $clave == $tipoSeleccionado) {
                $selected='selected';
            }
            $html .= "<option value=\"{$clave}\"{$selected}>{$valor}</option>";
        }
        $html .= '</select>';

        return $html;
    }

    protected function procesaFormulario(&$datos)
    {
        $this->errores = [];

        // Verificamos que la subida ha sido correcta
        $ok = $_FILES['archivo']['error'] == UPLOAD_ERR_OK && count($_FILES) == 1;
        if (! $ok ) {
            $this->errores['archivo'] = 'Error al subir el archivo';
            return;
        }  

        $nombre = $_FILES['archivo']['name'];

        $tipoAlmacen = $datos['tipo'] ?? Imagen::PUBLICA;

        $ok = array_key_exists($tipoAlmacen, self::TIPO_ALMACEN);
        if (!$ok) {
            $this->errores['tipo'] = 'El tipo del almacén no está seleccionado o no es correcto';
        }

        /* 1.a) Valida el nombre del archivo */
        $ok = self::check_file_uploaded_name($nombre) && $this->check_file_uploaded_length($nombre);

        /* 1.b) Sanitiza el nombre del archivo (elimina los caracteres que molestan)
        $ok = self::sanitize_file_uploaded_name($nombre);
        */

        /* 1.c) Utilizar un id de la base de datos como nombre de archivo */
        // Vamos a optar por esta opción que es la que se implementa más adelante

        /* 2. comprueba si la extensión está permitida */
        $extension = pathinfo($nombre, PATHINFO_EXTENSION);
        $ok = $ok && in_array($extension, self::EXTENSIONES_PERMITIDAS);

        /* 3. comprueba el tipo mime del archivo corresponde a una imagen image/* */
        $finfo = new \finfo(FILEINFO_MIME_TYPE);
        $mimeType = $finfo->file($_FILES['archivo']['tmp_name']);
        $ok = $ok && preg_match('/image\/.+/', $mimeType) === 1;

        if (!$ok) {
            $this->errores['archivo'] = 'El archivo tiene un nombre o tipo no soportado';
        }

        if (count($this->errores) > 0) {
            return;
        }

        $tmp_name = $_FILES['archivo']['tmp_name'];

        $imagen = Imagen::crea($nombre, $mimeType, $tipoAlmacen, '');
        $imagen->guarda();
        $fichero = "{$imagen->id}.{$extension}";
        $imagen->setRuta($fichero);
        $imagen->guarda();
        $ruta = implode(DIRECTORY_SEPARATOR, [RUTA_ALMACEN_PUBLICO, $fichero]);
        if ($tipoAlmacen == Imagen::PRIVADA) {
            $ruta = implode(DIRECTORY_SEPARATOR, [RUTA_ALMACEN_PRIVADO, $fichero]);
        }
        if (!move_uploaded_file($tmp_name, $ruta)) {
            $this->errores['archivo'] = 'Error al mover el archivo';
        }
    }


    /**
     * Check $_FILES[][name]
     *
     * @param (string) $filename - Uploaded file name.
     * @author Yousef Ismaeil Cliprz
     * @See http://php.net/manual/es/function.move-uploaded-file.php#111412
     */
    private static function check_file_uploaded_name($filename)
    {
        return (bool) ((preg_match('/^[0-9A-Z-_\.]+$/i', $filename) === 1) ? true : false);
    }

    /**
     * Sanitize $_FILES[][name]. Remove anything which isn't a word, whitespace, number
     * or any of the following caracters -_~,;[]().
     *
     * If you don't need to handle multi-byte characters you can use preg_replace
     * rather than mb_ereg_replace.
     * 
     * @param (string) $filename - Uploaded file name.
     * @author Sean Vieira
     * @see http://stackoverflow.com/a/2021729
     */
    private static function sanitize_file_uploaded_name($filename)
    {
        /* Remove anything which isn't a word, whitespace, number
     * or any of the following caracters -_~,;[]().
     * If you don't need to handle multi-byte characters
     * you can use preg_replace rather than mb_ereg_replace
     * Thanks @Łukasz Rysiak!
     */
        $newName = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $filename);
        // Remove any runs of periods (thanks falstro!)
        $newName = mb_ereg_replace("([\.]{2,})", '', $newName);

        return $newName;
    }

    /**
     * Check $_FILES[][name] length.
     *
     * @param (string) $filename - Uploaded file name.
     * @author Yousef Ismaeil Cliprz.
     * @See http://php.net/manual/es/function.move-uploaded-file.php#111412
     */
    private function check_file_uploaded_length($filename)
    {
        return (bool) ((mb_strlen($filename, 'UTF-8') < 250) ? true : false);
    }
}
