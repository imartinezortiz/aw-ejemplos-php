<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/estilos.css?20220304" />
    <title>Gestión de archivos</title>
</head>
<body>
    <h1>Gestión de archivos</h1>

    <h2>Subida de archivos a través de formularios</h2>

    <p>Habitualmente tenemos que incluir la subida de archivos como parte de una aplicación web. Estos archivos pueden ser de diferentes tipos, pero es habitual permitir subir imágenes y otros recursos multimedia.</p>

    <p><a href="ejemplo.php">Este ejemplo</a> permite subir una imagen al servidor y además mostrarla, para ver como sería el proceso de gestión. Puedes además subirla a diferentes &quot;almacenes&quot; de modo que puedes controlar el acceso a las imágenes de modo que no todo el mundo pueda acceder a todas las imágenes.</p>

    <p>Antes de poder utilizar el ejemplo, tienes que importar los siguientes ficheros SQL</p>
    <ol>
        <li><a href="mysql/01-estructura.sql">01-estructura.sql</a>. Que crea la base de datos y la tabla que hace falta para el ejemplo.</li>
        <li><a href="mysql/02-usuario.sql">02-usuario.sql</a>. Que crea el usuario que hace falta para el ejemplo.</li>
    </ol>

</body>
</html>