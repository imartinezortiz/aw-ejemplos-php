function getParams() {
    return parseParams(window.location.search);
}

function getHashParams(){
    let params = window.location.hash;
    if (params.length > 0) {
        // Convertimos #param1=value1&param2=value2 en ?param1=value1&param2=value2
        params = params.substring(1);
        params = '?'+params;
    }
    return parseParams(params);
}

function parseParams(params) {
    let hashParams = {};
    if (window.URLSearchParams) {
        let searchParams = new URL(params, window.location.href).searchParams
        for (p of searchParams){
            hashParams[p[0]] = p[1];
        }
    } else { // Si el navegador no tiene la clase URLSearchParams
        let e,  
            a = /\+/g,  // Regex for replacing addition symbol with a space
            r = /([^&;=]+)=?([^&;]*)/g,
            d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
            q = params.substring(1);

        while (e = r.exec(q))
        hashParams[d(e[1])] = d(e[2]);
    }
    return hashParams;
}

function getURLSearch(url){
    let queryParams = null;
    if ( window.URL) {
        queryParams = (new URL(url, window.location.href)).search;
    } else { // Si el navegador no tiene la clase URL
        let queryRe = /[^\?]+(\?.*)/;
        let match;
        if ((match = queryRe.exec(url)) !== null) {
            queryParams = match[1];
        }
    } 
    return queryParams;
    
}

const INTERVALO_REFRESCO = 20*1000;
let editando = false;
let timer = null;


editando = 'EDITANDO' in getParams();

if (editando) {
    timer = setInterval(()=>{
        window.location.reload();
    }, INTERVALO_REFRESCO);
}