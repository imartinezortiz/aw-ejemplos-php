<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejemplo de página</title>
</head>
<body>
<?php include './includes/vistas/cabecera.php' ?>
<h1>Ejemplo de página</h1>
<main>
    <p>Contenido principal de la página</p>
</main>
<?php include './includes/vistas/footer.php' ?>
</body>
</html>