<?php
require_once 'Template.php';
use es\ucm\fdi\aw\Template;

/* indexColors.html debe estar dentro de un directorio 'views' dentro del directorio donde
 * se encuentra este fichero PHP.
 */
$template = new Template('indexColors.html');
$template->render([
    'title' => 'Home Page',
    'colors' => ['red','blue','green']
]);