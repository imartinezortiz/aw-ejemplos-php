<?php
@include __DIR__.'/includes/helpers/'. pathinfo(__FILE__, PATHINFO_FILENAME) . 'Helper.php';
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/estilos.css?20220304" />
    <script src="../assets/js/main.js"></script>
    <title>Generación HTML PHP</title>
</head>
<body>
    <h1>Generación de HTML en PHP</h1>

    <p>Los mecanismos que hemos visto en clase para organizar la generación de HTML en PHP han sido:</p>
    <ul>
        <li><em>Incluir el HTML fuera de los bloques php <code>&lt;php ?&gt;</code></em>. Este es el mecanismo más básico y efectivo aunque es el más limitado cuando parte del HTML requiere de datos procesados con PHP. Normalmente utilizamos los bloques <code>&lt;?= ?&gt;</code> para ayudarnos a la hora de incluir los datos dinámicos en el HTML.</li>
        <li><em>Utilizar <code>echo</code></em>. Est es el mecanismo más primitivo que tenemos pero puede ser muy potente combinando la interpolación de cadenas y el uso de <a href="../peticiones/index.php#cadenas-multilinea">cadenas multi-línea</a>.</li>
        <li><em>Dividir las páginas en elementos reutilizables y utilizar <code>include()</code> para componer la página completa</em>. Esta es la aproximación del ejercicio 2 y, aunque funciona, tiene sus limitaciones ya que seguimos teniendo bastante secciones de HTMl repetidas.</li>
    </ul>

    <p>Estos problemas no son nuevos en el desarrollo de aplicaciones web y, más concretamente, en PHP. Una manera más avanzada consiste en integrar dentro de la aplicación algún sistema de plantillas como <a href="https://twig.symfony.com/">Twig</a>, <a href="https://book.cakephp.org/">CakePHP</a>, etc. No obstante, entre la generación más rudimentaria de generación de HTML y un sistema de plantillas completo, tenemos alguna posibilidad intermedia.</p>

    <h2>Modularización de páginas avanzada</h2>

    <p>Partiendo de las ideas del ejercicio 2, y con un ejemplo como el siguiente</p>
    
    <section class="ejemplo">
        <h6><a href="pagina.php">pagina.php</a></h6>
        <pre><code><?= htmlspecialchars($pagina, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>
    
    <section class="ejemplo">
        <h6>cabecera.php</h6>
        <pre><code><?= htmlspecialchars($cabecera, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>
    
    <section class="ejemplo">
        <h6>footer.php</h6>
        <pre><code><?= htmlspecialchars($footer, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>

    <p>Si tenemos que crear otra página login.php, ésta tendrá un contenido muy parecido al de index.php, normalmente cambiando el contenido que se encuentra entre los <code>include</code>.</p>

    <section class="ejemplo">
        <h6><a href="login.php">login.php</a></h6>
        <pre><code><?= htmlspecialchars($login, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>

    <p>Esta repetición / redundancia además puede dificultar el desarrollo ya que si queremos cambiar algún aspecto general / estructura de la página, tendríamos que ir vista a vista para realizar las modificaciones.</p>

    <p>Podemos solventar este problema de manera básica, creando un fichero plantilla y utilizar o bien la interpolación de cadenas, o los bloques simplificados PHP (<code>&lt;?= ?&gt;</code>) para generar aquellos aspectos que sean dinámicos en la página que nos interesa, utilizando las cadenas multi-linea. El ejemplo anterior quedaría del siguiente modo (cabecera.php y footer.php no se modifican):</p>
   
    <section class="ejemplo">
        <h6>plantilla.php</h6>
        <pre><code><?= htmlspecialchars($plantilla, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>
   
    <section class="ejemplo">
        <h6><a href="paginaPlantilla.php">paginaPlantilla.php</a></h6>
        <pre><code><?= htmlspecialchars($paginaPlantilla, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>

    <section class="ejemplo">
        <h6><a href="loginPlantilla.php">loginPlantilla.php</a></h6>
        <pre><code><?= htmlspecialchars($login, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>

    <h2>Motor de plantilla básico</h2>

    <p>Con las modificación anterior para las páginas ya podemos evitar las redundancias y podemos aplicarlo perfectamente en nuestros proyectos. No obstante, podemos ir un paso más allá y utilizar un motor de plantillas simplificado que nos permita mejorar la usabilidad de la aproximación anterior. En particular, al utilizar cadenas multi-líneas perdíamos las ayudas de nuestro entorno de desarrollo para la creación de HTML.</p>

    <p>Con esta idea en mente podemos crear un <a href="../highlight.php?file=plantillas/includes/src/Template.php">pequeño motor de plantillas encapsulado en una clase PHP</a>.</p>

    <h3>¿Cómo se usa?</h3>
 
    <p>Crea el fichero PHP donde quieras utilizarla, por ejemplo, <code>usoPlantilla.php</code></p>
    <section class="ejemplo">
        <h6>usoPlantilla.php</h6>
        <pre><code><?= htmlspecialchars($usoMotorPlantilla, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>

    <p>Crear un fichero HTML y dale el nombre <code>layout.html</code>. Este fichero contendrá la estructura (layout) de nuestras vistas.</p>
     
    <section id="layout" class="ejemplo">
        <h6>layout.html</h6>
        <pre><code><?= htmlspecialchars($layoutHTML, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>

    <p>Ahora vamos a crear una fichero HTML para la vista específica <code>index.html</code></p>
     
    <section class="ejemplo">
        <h6>index.html</h6>
        <pre><code><?= htmlspecialchars($indexHTML, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>

    <p>Una vez creado estos ficheros si visitas <code>usoPlantilla.php</code>, verás algo similar a:</p>

    <section id="index" class="ejemplo">
        <h6>Resultado al visitar usoPlantilla.php</h6>
        <pre><code><?= htmlspecialchars($usoMotorPlantillaEvaluada, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>

    <p>Normalmente, tendremos que pasar parámetros a las plantillas con los datos específicos que hemos calculado o sacado de la BD previamente, por ejemplo del siguiente modo:</p>

    <section class="ejemplo">
        <h6>usoPlantillaParametros.php</h6>
        <pre><code><?= htmlspecialchars($usoMotorPlantillaParametros, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>

    <p>Y teniendo una vista <code>indexColors.html</code> con la siguiente estructura</p>
       
    <section class="ejemplo">
        <h6>indexColors.html</h6>
        <pre><code><?= htmlspecialchars($indexColorsHTML, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>

    <section id="index-colors" class="ejemplo">
        <h6>Resultado al evaluar la nueva plantilla</h6>
        <pre><code><?= htmlspecialchars($usoMotorPlantillaParametrosEvaluada, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>

    <h3>Salida no filtrada</h3>

    <p>Por defecto, toda la salida que se genera en las plantillas y a través de variables PHP es filtrada previamente con la función <code>htmlspecialchars()</code> para evitar que los caracteres especiales se interpreten al generar la salida.</p>

    <p>En algunas circunstancias es necesario generar salida sin filtrar. Para lograrlo es necesario cambiar <code>{{ $output }}</code> por <code>{{{ $output }}}</code>.</p>

    <aside class="advertencia">Recuerda que si no filtras la salida del usuario pueden suceder cosas no esperadas como potenciales ataques <a href="https://owasp.org/www-community/attacks/xss/">XSS</a>. El filtrado es una medida adicional que debemos de incluir además de la validación de los datos que nos llegan del usuario.</aside>

    <h3>Extensión de bloques</h3>

    <p>Normalmente el comportamiento el comportamiento es que si definimos un bloque en una plantilla y posteriormente lo volvemos a definir en otra plantilla o vista, el contenido inicialmente definido se pierde.</p>

    <p>No obstante, en algunas ocasiones no queremos que este contenido original se pierda, sino que nuestro objetivo es añadir contenido adicional. Para lograrlo, tenemos que incluir la cadena <code>@parent</code> dentro de la redefinición del bloque.</p>

    <p>Por ejemplo, en <a href="#layout"><code>layout.html</code></a> está definido el bloque <em>header</em>, y este bloque se redefine en <a href="#index"><code>index.html</code></a>, perdiendo la cabecera <code>&lt;h1&gt;Welcome !&lt;/h1&gt;</code> por el camino.</p>

    <p>Por el contrario en la vista <a href="#index-colors"><code>indexColors.html</code></a>, el bloque se <em>extiende</em> y se mantiene el contenido original además de añadirse el nuevo contenido.</p>

    <h3>Incluir otras plantillas</h3>

    <p>Podemos incluir otras plantillas adicionales dentro de otra plantilla utilizando <code>{{% include otraPlantilla.html %}}</code></p>

    <h3>Gestión de las plantillas compiladas</h3>

    <p>Por defecto, el motor de plantillas está configurado para generar siempre compilar las plantillas ya que más útil de este modo cuando estamos desarrollando. No obstante, el proceso de compilación no es despreciable, por lo que es posible configurar el motor de plantillas para utilizar las plantillas compiladas.</p>

    <p>Aunque el motor de plantillas, verifica que la plantilla es suficientemente nueva (la fecha de modificación de la plantilla compilada es posterior al fichero de la plantilla), puede ser necesario vaciar la cache manualmente. Se puede borrar manualmente el contenido del directorio de cache, o bien utilizar el método <code>$template->clearCache()</code></p>

    <section id="index-colors" class="ejemplo">
        <h6>Configuración de la cache y limpieza</h6>
        <pre><code><?= htmlspecialchars($configuracionMotorPlantillas, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>
</body>
</html>
