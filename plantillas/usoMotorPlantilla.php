<?php
require_once 'Template.php';
use es\ucm\fdi\aw\Template;

/* index.html debe estar dentro de un directorio 'views' dentro del directorio donde
 * se encuentra este fichero PHP.
 */
$template = new Template('index.html');
$template->render();