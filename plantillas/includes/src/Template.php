<?php
namespace es\ucm\fdi\aw;

/**
 * Motor de plantilla simplificado.
 *
 * @author Ivan Martinez-Ortiz
 * @author David Adams
 * @see https://codeshack.io/lightweight-template-engine-php/
 * @license Released under the MIT License.
 */
class Template {

    /**
     * Opciones de configuración por defecto
     * 
     * cache.path: Ruta donde se almacenarán las plantillas &quot;compiladas&quot;
     * cache.enable: Si <code>true</code> entonces se utilizará el fichero compilado de la plantilla si existe, en otro caso se compilará.
     * views.path: Ruta base donde se encuentran todas las plantillas o archivos auxiliares para las plantillas.
     * views.encoding: Codificación utilizada al crear los archivos de plantilla.
     */
    const DEFAULT_OPTIONS = [
        'cache' =>  [
            'enabled' => false,
            'path' => 'cache/'
        ],
        'views' => [
            'path' => 'views/',
            'encoding' => 'UTF-8'
        ]
    ];

    private static $options;

    private static $cachePath;

    private static $viewsRootPath;

    private static $viewsEncoding;

    private static $cacheEnabled;

    /**
     * Inicializa el motor de plantillas
     * 
     * @param {array}  $options Opciones de configuración para el motor de plantillas.
     */
    public static function config($options = []) {
        self::$options = array_merge(self::DEFAULT_OPTIONS, $options);

        self::$cacheEnabled = boolval(self::$options['cache']['enabled']);
        self::$cachePath = self::$options['cache']['path'];
        $pathLength = mb_strlen(self::$cachePath);
        if ( $pathLength > 0 && mb_substr(self::$cachePath,  $pathLength-1, 1) != DIRECTORY_SEPARATOR) {
            self::$cachePath .= DIRECTORY_SEPARATOR;
        }
        if (!file_exists(self::$cachePath)) {
            mkdir(self::$cachePath, 0744);
        }
        self::$viewsRootPath = self::$options['views']['path'];
        self::$viewsEncoding = self::$options['views']['encoding'];
    }

    private $file;

    /**
     * Crea una plantilla. Una vez creada, puede reutilizarse múltiples veces.
     * 
     * @param {string} $file Ruta a la plantilla. Esta ruta debe ser relativa a la ruta especificada en el parámetro <code>views.path</code>
     */
    public function __construct($file) {
        $this->file = $file;
        $this->blocks = array();
    }

    /**
     * Genera el HTML asociado a la plantilla.
     */
    public function render($data = array()) {
        $cached_file = $this->cache($this->file);
        extract($data, EXTR_SKIP);
        require $cached_file;
    }

    /**
     * Elimina la plantilla compilada.
     */
    public function clearCache() {
        unlink(self::$cachePath . $this->file);
    }

    /**
     * Elimina todas las plantillas compiladas.
     */
    public function clearCacheAll() {
        foreach(glob(self::$cachePath . '*') as $file) {
            unlink($file);
        }
    }

    private function cache($file) {

        $cached_file = self::$cachePath . str_replace(array(DIRECTORY_SEPARATOR, '.html'), array('_', ''), $file . '.php');
        if (!self::$cacheEnabled || !file_exists($cached_file) || filemtime($cached_file) < filemtime($file)) {
            $code = $this->includeFiles($file);
            $code = $this->compileCode($code);
            file_put_contents($cached_file, '<?php class_exists(\'' . __CLASS__ . '\') or exit; ?>' . PHP_EOL . $code);
        }
        return $cached_file;
    }

    private function viewPath($file) {
        if (mb_strlen($file) > 0 && mb_substr($file, 0, 1) != DIRECTORY_SEPARATOR) {
            return self::$viewsRootPath . DIRECTORY_SEPARATOR . $file;
        }
        return $file;
    }

    private function includeFiles($file) {
        $code = file_get_contents($this->viewPath($file));
        preg_match_all('/{% ?(extends|include) ?\'?(.*?)\'? ?%}/i', $code, $matches, PREG_SET_ORDER);
        foreach ($matches as $value) {
            $code = str_replace($value[0], $this->includeFiles($value[2]), $code);
        }
        $code = preg_replace('/{% ?(extends|include) ?\'?(.*?)\'? ?%}/i', '', $code);
        return $code;
    }

    private function compileCode($code) {
        $code = $this->compileBlock($code);
        $code = $this->compileYield($code);
        $code = $this->compileEscapedEchos($code);
        $code = $this->compileVerbatimEchos($code);
        $code = $this->compilePHP($code);
        return $code;
    }

    private function compilePHP($code) {
        return preg_replace('~\{%\s*(.+?)\s*\%}~is', '<?php $1 ?>', $code);
    }

    private function compileVerbatimEchos($code) {
        return preg_replace('~\{{{\s*(.+?)\s*\}}}~is', '<?php echo $1 ?>', $code);
    }

    private function compileEscapedEchos($code) {
        return preg_replace('~\{{\s*(.+?)\s*\}}~is', '<?php echo htmlspecialchars($1, ENT_QUOTES,'.self::$viewsEncoding.') ?>', $code);
    }

    private function compileBlock($code) {
        preg_match_all('/{% ?block ?(.*?) ?%}(.*?){% ?endblock ?%}/is', $code, $matches, PREG_SET_ORDER);
        foreach ($matches as $value) {
            // Initialize block content
            if (!array_key_exists($value[1], $this->blocks)) {
                $this->blocks[$value[1]] = '';
            }
            if (strpos($value[2], '@parent') === false) {
                $this->blocks[$value[1]] = $value[2];
            } else {
                $this->blocks[$value[1]] = str_replace('@parent', $this->blocks[$value[1]], $value[2]);
            }
            $code = str_replace($value[0], '', $code);
        }
        return $code;
    }

    private function compileYield($code) {
        foreach($this->blocks as $block => $value) {
            $code = preg_replace('/{% ?yield ?' . $block . ' ?%}/', $value, $code);
        }
        $code = preg_replace('/{% ?yield ?(.*?) ?%}/i', '', $code);
        return $code;
    }

}
