<?php
require_once 'includes/config.php';

/*
 * FUNCIONES DE APOYO
 */

/*
 * LOGICA PRINCIPAL
 */
$pagina = file_get_contents('pagina.php');
$cabecera = file_get_contents('includes/vistas/cabecera.php');
$footer = file_get_contents('includes/vistas/footer.php');
$login = file_get_contents('login.php');

$plantilla = file_get_contents('includes/vistas/plantilla.php');
$paginaPlantilla = file_get_contents('paginaPlantilla.php');
$loginPlantilla = file_get_contents('loginPlantilla.php');


$usoMotorPlantilla = file_get_contents('usoMotorPlantilla.php');
$layoutHTML = file_get_contents('includes/vistas/layout.html');
$indexColorsHTML = file_get_contents('includes/vistas/indexColors.html');
$indexHTML = file_get_contents('includes/vistas/index.html');

$usoMotorPlantillaCodigo = <<<'EOS'
require_once 'includes/src/Template.php';
use es\ucm\fdi\aw\Template;

Template::config([
    'views' => [ 'path' => 'includes/vistas/'],
    'cache' => [ 'path' => 'includes/cache/'],
]);

$template = new Template('index.html');
$template->render();
EOS;

ob_start();
// XXX: OJO eval() es MUY peligroso (sobre todo cuando se utilizan datos que vienen del usuario) y no se debería de usar. Aquí tiene sentido para generar la salida del ejemplo
eval($usoMotorPlantillaCodigo);
$usoMotorPlantillaEvaluada = ob_get_clean();

$usoMotorPlantillaParametros = file_get_contents('usoMotorPlantillaParametros.php');
$usoMotorPlantillaParametrosCodigo = <<<'EOS'
require_once 'includes/src/Template.php';
use es\ucm\fdi\aw\Template;

Template::config([
    'views' => [ 'path' => 'includes/vistas/'],
    'cache' => [ 'path' => 'includes/cache/'],
]);
$template = new Template('indexColors.html');
$template->render([
    'title' => 'Home Page',
    'colors' => ['red','blue','green']
]);
EOS;

ob_start();
// XXX: OJO eval() es MUY peligroso (sobre todo cuando se utilizan datos que vienen del usuario) y no se debería de usar. Aquí tiene sentido para generar la salida del ejemplo
eval($usoMotorPlantillaParametrosCodigo);
$usoMotorPlantillaParametrosEvaluada = ob_get_clean();


$configuracionMotorPlantillas = <<<'EOS'
<?php

require_once 'includes/src/Template.php';
use es\ucm\fdi\aw\Template;

Template::config([
    'views' => [ 'path' => 'includes/vistas/'],
    'cache' => [
        'path' => 'includes/cache/',
        'cache' => true
    ],
]);

$template = new Template('indexColors.html');
$template->render([
    'title' => 'Home Page',
    'colors' => ['red','blue','green']
]);

// Elimina plantilla compilada asociada a 'indexColors.html'
$template->clearCache();

// Elimina todas las plantillas compiladas
Template::clearCacheAll();
EOS;