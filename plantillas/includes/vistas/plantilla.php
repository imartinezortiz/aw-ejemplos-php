<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title ?></title>
</head>
<body>
<?php include './includes/vistas/cabecera.php' ?>
<?= $contenidoPrincipal ?>
<?php include './includes/vistas/footer.php' ?>
</body>
</html>