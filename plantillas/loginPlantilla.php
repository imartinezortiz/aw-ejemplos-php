<?php

$title = 'Página de login';
$contenidoPrincipal = <<<EOS
<h1>Formulario de Login</h1>
<main>
    <form>
        <div>
            <label for="usuario">Usuario:</label><input type="text" id="usuario">
        </div>
        <div>
            <label for="password">Contraseña:</label><input type="password" id="password">
        </div>
        <div><button>Conectar</button></div>
    </form>
</main>
EOS;

include 'includes/vistas/plantilla.php';