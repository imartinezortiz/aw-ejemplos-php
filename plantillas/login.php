<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejemplo de página</title>
</head>
<body>
<?php include './includes/vistas/cabecera.php' ?>
<h1>Formulario de Login</h1>
<main>
    <form>
        <div>
            <label for="usuario">Usuario:</label><input type="text" id="usuario">
        </div>
        <div>
            <label for="password">Contraseña:</label><input type="password" id="password">
        </div>
        <div><button>Conectar</button></div>
    </form>
</main>
<?php include './includes/vistas/footer.php' ?>
</body>
</html>