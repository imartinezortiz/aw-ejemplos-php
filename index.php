<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/estilos.css?20220304" />
    <title>Notas y ejemplos PHP</title>
</head>
<body>
    <h1>Notas y ejemplos de utilidad PHP</h1>
    <p>En las siguientes páginas podrás encontrar algunas notas y ejemplos que te pueden ser de utilidad para mejorar tus habilidades de PHP, de manera que te sea más fácil la programación de los diferentes ejercicios y prácticas de la asignatura.</p>
    <p>Los ejemplos son los siguientes:</p>
    <ul>
        <li><a href="peticiones/index.php">Sobre la gestión de peticiones en PHP</a>. Recuerda algunos aspectos a tener en cuenta a la hora de gestionar peticiones en scripts PHP e introduce algunos nuevos mecanismos de PHP para simplificar y gestionar de manera más efectiva el código PHP.</li>

        <li><a href="formularios/index.php?202220304">Gestión formularios</a>. Analiza con detalle y con ejemplos concretos el procesamiento de formularios en PHP.</li>
        <ul>
            <li>Este ejemplo muestra qué sucede cuando una formulario <a href="formularios/transferencia.php?202220304">no se gestiona con una petición <code>$_POST</code></a>.</li>
            <li>Cómo pasar un <a href="formularios/index.php#multiples-valores">parámetro con múltiples valores</a>.</li>
        </ul>

        <li><a href="sesiones/index.php?20220303">Gestión de sesiones en PHP</a>. Recordatorio de los aspectos principales de gestión de sesiones en PHP.</li>

        <li><a href="plantillas/index.php">Generación de HTML con plantillas</a>. La generación de HTML en la aplicación puede ser un poco repetitiva y farragosa en cuanto la estructura HTML de las páginas empieza a complicarse. En esta sección exploraremos algunas maneras de abordar este problema.</li>

        <li><a href="configPHP.php">Configuración de PHP</a>. El motor de PHP tiene numerosos parámetros que controlan el comportamiento y la ejecución del motor, siendo necesario algunas veces tener que modificar los valores por defecto o de la instalación que utilizamos.</li>

        <li><a href="funciones/index.php">Características avanzadas de las funciones</a>. Las funciones PHP tienen algunas características avanzadas o distintivas comparadas con otros lenguajes de programación, por ejemplo, los <em>closures</em>.</li>

        <li><a href="archivos/index.php">Subida de archivos con formularios</a>. Ejemplo de subida de imágenes a través de un formulario y cómo se pueden utilizar / gestionar en la aplicación.</li>
    </ul>

    <aside class="consejo">Los hiperenlaces cambiarán de color según los hayas visitado (<span class="visited-link">en verde</span>) o no (<span class="new-link">en rojo</span>),. Si la página ha cambiado desde tu última visita, el hiperenlace volverá a cambiar de color.</aside>

</body>
</html>
