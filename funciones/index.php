<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/estilos.css?20220304" />
    <title>Funciones (avanzadas)</title>
</head>
<body>
    <h1>Características avanzadas de las funciones</h1>
    
    <p>Las funciones PHP tienen algunas características avanzadas o distintivas comparadas con otros lenguajes de programación y que te pueden ser de gran ayuda a la hora de programar.</p>

    <nav>
        <p>Contenidos de la página</p>
        <ul>
            <li><a href="#argumentos-variables">Número variable de argumentos</a>.</li>
            <li><a href="#invocar-funciones">Usar una función o método de un objeto a través de una variable que contiene su nombre o través de otra función</a>. En PHP es habitual utilizar este mecanismo para implementar callbacks (<em>callable</em> en el contexto PHP).</li>
            <li><a href="#closures">Funciones anónimas y <em>closures</em></a>. Permiten definir una función bajo demanda para pasar como un parámetro.</li>
        </ul>
    </nav>

    <h2 id="argumentos-variables">Número variable de argumentos</h2>

    <p>Desde los inicios de PHP se han admitido un número variable de argumentos mediante el uso de <code>func_num_args()</code>, <code>func_get_arg()</code> y <code>function_get_args()</code> aunque ha sido considerado una mala práctica.</p>

    <p>A partir de PHP 7.0 se ha añadido <a href="https://www.php.net/manual/en/functions.arguments.php#functions.variable-arg-list">el modificador <code>...</code></a> aplicable a los parámetros de las funciones que permite tratar el parámetro formal de la función como si fuera un array. Nótese que este modificador sólo se puede aplicarse a 1 parámetro y éste debe colocarse el último si la función tiene mas de uno.</p>

    <section class="ejemplo">
        <h6>Función con número variable de argumentos</h6>
<?php

$ejemploFunctionArgs = <<<'EOS'
function sumar(...$numeros) {
    $aux = 0;
    foreach ($numeros as $n) {
        $aux += $n;
    }
    return $aux;
}

echo sumar(1, 2, 3, 4)."\n";

function suma($a, $b) {
    return $a + $b;
}

// ... se puede usar para "desestructurar" un array en sus componentes individuales
echo suma(...[1, 2])."\n";

$a = [1, 2];
echo suma(...$a)."\n";
EOS;

?>
        <pre><code><?= htmlspecialchars($ejemploFunctionArgs, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
        <p>Generando la siguiente salida:</p>
        <pre><samp><?php
            // XXX: OJO eval() es MUY peligroso (sobre todo cuando se utilizan datos que vienen del usuario) y no se debería de usar. Aquí tiene sentido para generar la salida del ejemplo
            eval($ejemploFunctionArgs)
        ?></samp></pre>

    </section>

    <h2 id="invocar-funciones">Invocar funciones a través de variables y otras funciones</h2>

    <p>Las funciones y métodos en PHP además de invocarse como en otros lenguajes de programación, en PHP también se puede utilizar:</p>
    
    <ul>
        <li><strong>Una variable</strong> que contenga el nombre de la función o método para poder invocarla.</li>
        <li>Usando <a href="https://www.php.net/manual/en/function.call-user-func"><code>call_user_func()</code></a>.</li>
    </ul>
    
    <p>Las aplicaciones de estas capacidades son múltiples, por ejemplo, algunas funciones de PHP requiere que les pasemos como parámetro una función para poder realizar su trabajo como <a href="https://www.php.net/manual/en/function.usort.php"><code>usort()</code></a>.</p>

    <aside class="advertencia">En el pasado en diversas aplicaciones PHP, se ha utilizado este mecanismo para facilitar el desarrollo de la aplicación en diferentes módulos, pero también ha generado problemas de seguridad porque no se han validado adecuadamente los parámetros.</aside>

    <section class="ejemplo">
        <h6>Invocando funciones</h6>
<?php
$ejemploFuncionCodigoComun = <<<'EOS'
function imprime($mensaje) 
{
    echo __FUNCTION__." : {$mensaje}\n";
}

class Prueba
{
    public static function metodoEstaticoPublico()
    {
        echo "Llamando a ". __METHOD__ . "\n";
    }

    private static function metodoEstaticoPrivado()
    {
        echo "Llamando a ". __METHOD__ . "\n";
    }

    private function metodoPrivado()
    {
        echo "Llamando a ". __METHOD__ . "\n";
    }

    public function metodoPublico()
    {
        echo "Llamando a ". __METHOD__ . "\n";
    }
}

EOS;

$ejemploFunctionArgs = $ejemploFuncionCodigoComun . <<<'EOS'

echo "\n// Invocando función de la manera habitual\n";
imprime('Hola mundo');

echo "\n// Invocando métodos de la manera habitual\n";
Prueba::metodoEstaticoPublico();
try {
    Prueba::metodoEstaticoPrivado();
} catch (Throwable $error) {
    echo "Lo esperable al invocar un método privado\n";
}

$prueba = new Prueba();
$prueba->metodoPublico();
try {
    $prueba->metodoPrivado();
} catch (Throwable $error) {
    echo "Lo esperable al invocar un método privado\n";
}


echo "\n// Invocando función usando una variable\n";

$nombreFuncion = 'imprime';
$nombreFuncion('Mensaje a imprimir');


echo "\n// Invocando método usando una variable\n";

$nombreMetodo = 'metodoPublico';
$prueba->$nombreMetodo();

$nombreMetodo = 'metodoEstaticoPublico';
Prueba::$nombreMetodo();

$nombreMetodo = 'Prueba::metodoEstaticoPublico';
$nombreMetodo();

try {
    $nombreMetodo = 'metodoPrivado';
    $prueba->$nombreMetodo();
} catch (Throwable $error) {
    echo "Lo esperable al invocar un método privado\n";
}
EOS;

?>
        <pre><code><?= htmlspecialchars($ejemploFunctionArgs, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
        <p>Generando la siguiente salida:</p>
        <pre><samp><?php
            // XXX: OJO eval() es MUY peligroso (sobre todo cuando se utilizan datos que vienen del usuario) y no se debería de usar. Aquí tiene sentido para generar la salida del ejemplo
            eval($ejemploFunctionArgs)
        ?></samp></pre>

    </section>

    <p>También se ha utilizado (en la actualidad menos) para poder ejecutar una función / método, en base a un parámetro que llega desde el usuario, como <a href="index.php?modulo=ModA&func=imprime#ejemplo-dinamico">en este ejemplo</a> (o mejor <a href="./ModA/imprime#ejemplo-dinamico">con URLs bonitas</a>, sin .php).</p>

    <section id="ejemplo-dinamico" class="ejemplo">
        <h6>Invocación de métodos basada en la entrada del usuario</h6>
<?php
$ejemploInvocacionDinamica = <<<'EOS'
class ModA
{
    public static function imprime()
    {
        echo "Llamando a ". __METHOD__ . "\n";
    }
}

class ModB
{
    public static function imprime()
    {
        echo "Llamando a ". __METHOD__ . "\n";
    }
}

$modulo = filter_input(INPUT_GET, 'modulo', FILTER_SANITIZE_SPECIAL_CHARS);
$func = filter_input(INPUT_GET, 'func', FILTER_SANITIZE_SPECIAL_CHARS);

if (!$modulo || !$func) {
    // Error, volvemos a un punto seguro
    // header('Location: index.php');
    echo "\nOops\n";
    exit();
}

$metodo = "$modulo::$func";
if (! is_callable($metodo)) {
    // Error, volvemos a un punto seguro
    // header('Location: index.php');
    echo "\nOops\n";
    exit();
}

$modulo::$func();
print_r($_GET);
EOS;

?>
        <pre><code><?= htmlspecialchars($ejemploInvocacionDinamica, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
        <p>Generando la siguiente salida:</p>
        <pre><samp><?php
            // XXX: OJO eval() es MUY peligroso (sobre todo cuando se utilizan datos que vienen del usuario) y no se debería de usar. Aquí tiene sentido para generar la salida del ejemplo
            eval($ejemploInvocacionDinamica)
        ?></samp></pre>

    </section>

    <h3>Callables / callbacks</h3>

    <p>Partiendo de la idea del apartado anterior, en PHP se puede pasar una función a otra función como un <a href="https://www.php.net/manual/en/language.types.callable.php"><em>parámetro callable</em></a>.</p>

    <p>Además, esta funcionalidad no sólo aplica a las funciones sino también a los métodos de las clases. Aunque la sintaxis para invocar al método de un objeto (instancia de una clase)de una manera especial <em>mediante un array que agrupa el objeto y el método</em> (e.g. <code>[$prueba, 'metodoPublico']</code>).</p>    

    <section class="ejemplo">
        <h6>Pasando funciones como parámetro</h6>
<?php
$pasandoFunciones = <<<'EOS'
function mayorAMenor($a, $b) 
{
    return $b - $a;
}
class PruebaEspecial
{
    private function metodoPrivado()
    {
        echo "Llamando a ". __METHOD__ . "\n";
    }

    public function metodoPublico()
    {
        echo "Llamando a ". __METHOD__ . "\n";
    }

    public function metodoSorpresa()
    {
        echo "Sorpresa! ". __METHOD__ . "\n";
        return [$this, 'metodoPrivado'];
    }
}

echo "\n// Invocando método call_user_func('imprime', 'Qué útil call_user_func()')\n";
call_user_func('imprime', 'Qué útil call_user_func()');

echo "\n// Invocando método call_user_func([\$prueba, 'metodoPublico'])\n";
call_user_func([$prueba, 'metodoPublico']);

echo "\n// Utilizando directamente el nombre de la función\n";
$arr = [1, 2, 3];
usort($arr, 'mayorAMenor');
echo join(' ', $arr)."\n";

echo "\n// Utilizando una variable con el nombre de la función\n";
$arr = [1, 2, 3];
$comparador = 'mayorAMenor';
usort($arr, $comparador);
echo join(' ', $arr)."\n";


echo "\n// Intentando invocar un método privado desde fuera del objeto\n";
$pruebaEspecial = new PruebaEspecial();
try {
    $metodo = $pruebaEspecial->metodoSorpresa();
    $metodo();
} catch (Throwable $e) {
    echo "Tampoco se puede llamar a través de un callable / callback \n";
}
EOS;

?>
        <pre><code><?= htmlspecialchars($pasandoFunciones, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
        <p>Generando la siguiente salida:</p>
        <pre><samp><?php
            // XXX: OJO eval() es MUY peligroso (sobre todo cuando se utilizan datos que vienen del usuario) y no se debería de usar. Aquí tiene sentido para generar la salida del ejemplo
            eval($pasandoFunciones)
        ?></samp></pre>

    </section>

    <h2 id="closures">Funciones anónimas y Closures</h2>

    <p>En el apartado anterior hemos podido ver que no siempre es posible invocar un método de un objeto. Además, en el caso de utilizar la función a modo de <em>callback</em> en una función como <a href="https://www.php.net/manual/en/function.array-map"><code>array_map()</code></a>,esta función sólo recibe los elementos del array pero no podemos pasarles nosotros algún parámetro adicional. Por otro lado, con lo que hemos visto en los apartados anteriores, nos están obligando a definir una función simplemente para pasarla como parámetro.</p>

    <p>En PHP existe el concepto de <a href="https://www.php.net/manual/en/functions.anonymous.php">función anónima</a> que resuelve, entre otros, los problemas que acabamos de mencionar. Además las funciones anónimas son muy especiales porque son <a href="https://www.php.net/manual/en/class.closure.php"><em>Closures</em></a>, que como puede verse en el siguiente ejemplo, nos permite pasarle un &quot;contexto&quot; que tendrá disponible cuando se invoque.</p>

    <section class="ejemplo">
        <h6>Utilizando funciones anónimas y closures</h6>
<?php
$ejemploClosures = <<<'EOS'
echo "\n// Ordenando un array con usort y una función anónima como comparador\n";

$arr = [1, 2, 3];
usort($arr, function ($e1, $e2) {
    return $e2 - $e1;
});
echo join(' ', $arr)."\n";


echo "\n// Multiplicando por \$factorMultiplicador todos los elementos de un array\n";

$factorMultiplicador = 2;
$arr = [1, 2, 3];
$funcionAnonimaConClosure = function ($n) use ($factorMultiplicador) {
    return $n * $factorMultiplicador;
};
$arr = array_map($funcionAnonimaConClosure, $arr);
echo join(' ', $arr)."\n";


echo "\n// Invocando un método privado de un objeto !\n";
class PruebaClosure
{
    private $valor;

    public function __construct($valor)
    {
        $this->valor = $valor;
    }

    private function metodoPrivado()
    {
        echo "Llamando a ". __METHOD__ . ": {$this->valor}\n";
    }

    public function metodoEspecial()
    {
        echo "Esto es especial ". __METHOD__ . ": {$this->valor}\n";
        return Closure::fromCallable([$this, 'metodoPrivado']);
    }

    public function metodoEspecialAlternativo()
    {
        echo "Esto es especial alternativo". __METHOD__ . ": {$this->valor}\n";
        return function () {
            // $this queda vinculado automáticamente
            $this->metodoPrivado();
        };
    }
}

$prueba1 = new PruebaClosure(1);
$metodo = $prueba1->metodoEspecial();
$metodo();

echo "\n";

$metodo = $prueba1->metodoEspecialAlternativo();
$metodo();

echo "\n// Un closure es una instancia de la clase Closure de PHP, y podemos usar sus métodos\n";
$prueba2 = new PruebaClosure(2);
$metodo = $metodo->bindTo($prueba2);
$metodo();

EOS;

?>
        <pre><code><?= htmlspecialchars($ejemploClosures, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
        <p>Generando la siguiente salida:</p>
        <pre><samp><?php
            // XXX: OJO eval() es MUY peligroso (sobre todo cuando se utilizan datos que vienen del usuario) y no se debería de usar. Aquí tiene sentido para generar la salida del ejemplo
            eval($ejemploClosures)
        ?></samp></pre>

    </section>








</body>
</html>