<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/estilos.css?20220304" />
    <title>Sesiones</title>
</head>
<body>
    <h1>Gestión de sesiones en PHP</h1>

    <p>Las <a href="https://www.php.net/manual/en/intro.session.php">sesiones en PHP</a> son el principal mecanismo que vamos a utilizar en nuestras aplicaciones para &quot;acordarnos&quot; de ciertos datos entre peticiones realizadas desde el mismo navegador (que corresponderán al mismo usuario).</p>

    <aside class="consejo">Recuerda que para pasar información entre 2 páginas, puede ser más cómodo y eficiente o bien utilizar parámetros ocultos de un formulario (e.g <code>&lt;form type=&quot;hidden&quot; name=&quot;idProducto&quot; value=&quot;25&quot;&gt;</code>) o bien utilizar un parámetro que se pasa por la URL (e.g. <code>http://localhost/tienda/producto.php?idProducto=25</code>)</aside>

    <p>El punto de entrada para la gestionar la información que queremos guardar entre peticiones es la variable superglobal <code>$_SESSION</code>. No obstante, esta variable sólo está disponible una vez se ha inicializado la gestión de sesiones.</p>

    <section class="ejemplo">
        <h6>Intento de uso de la variables $_SESSION</h6>
<?php
$ejemploBasicoSesiones = <<<'EOS'
if (! isset($_SESSION)) {
    echo '<p>La sesión no está disponible</p>';
}
EOS;
?>
        <pre><code><?= htmlspecialchars($ejemploBasicoSesiones, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>

        <p>Interpretándose del siguiente modo la salida:</p>
        <div class="salida"><?php
            // XXX: OJO eval() es MUY peligroso (sobre todo cuando se utilizan datos que vienen del usuario) y no se debería de usar. Aquí tiene sentido para generar la salida del ejemplo
        eval($ejemploBasicoSesiones)
        ?></div>
    </section>

    <p>Dentro del módulo de gestión de sesiones, las que tenemos que utilizar necesariamente son:</p>
    <ul>
        <li><a href="https://www.php.net/manual/en/function.session-start.php"><code>session_start()</code></a>. Inicializa la gestión de sesiones creando una nueva o retomando la sesión existente. Recuerda que, por defecto, <strong>PHP usa cookies para gestionar las sesiones</strong> que viajan como parte de las cabeceras de respuesta y que, por tanto, sufren de los mismos problemas que al utilizar <code>header()</code> como se detalla en el <a href="../formularios/index.php#ejemplo-http">apartado de gestión de formularios</a>.</li>
        <li><a href="https://www.php.net/manual/en/function.session-destroy.php"><code>session_destroy()</code></a>. Elimina todos los datos asociados a la sesión, pero debes de tener en cuenta que <em>no elimina la cookie de sesión</em> ni hace limpieza en la variable <code>$_SESSION</code>, por lo que tendrás que tendrás que hacerlo tu mismo, al menos para <code>$_SESSION</code>. La documentación de <code>session_destroy()</code> proporcionar un <a href="https://www.php.net/manual/en/function.session-destroy.php#refsect1-function.session-destroy-examples">ejemplo detallado acerca de cómo hacer limpieza en la sesión</a> de manera correcta.</li>
    </ul>

    <h2>Recordatorio de puntos clave en la gestión de sesiones PHP </h2>

    <ul>
        <li>Inicializa la sesión <strong>al comienzo de todas las peticiones</strong>. Si vas a utilizar la variable <code>$_SESSION</code> tienes que ejecutar previamente <code>session_start()</code>. Para evitar problemas, es necesario que esta función se llame al comienzo de gestionar la petición del usuario normalmente <strong>al comienzo de tus scripts PHP</strong>. Puede ser cómodo inicializar la sesión como parte del proceso de inicialización de la aplicación <a href="#sesion-multiples-archivos">como se muestra en este ejemplo</a>, además así evitarás problemas.</li>
        <li><strong>No abuses de la información que guardas en la sesión</strong>. La sesiones consumen recursos en el servidor y una mala práctica en su uso puede llevar a que el servidor no pueda gestionar las peticiones de los clientes por falta de recursos. Almacena la información mínima que sea necesaria y <em>utiliza los otros mecanismos de paso de información por parámetro en las URLs o campos ocultos de formularios</em>.</li>
    </ul>
    <section id="sesion-multiples-archivos" class="ejemplo">
        <h6>Gestión de la sesión durante la inicializacíón sen tu aplicación</h6>
<?php
$sesionesEnMultiplesFicheros = <<<'EOS'
// config.php
<?php
// Otros aspectos de inicialziación de tu apliación
...
session_start();


// cabecera.php
<?php
require_once 'config.php';
?>
<header><?= isset($_SESSION['username']) ? "Bienvenido: {$_SESSION['username']}" : 'No estás conectado' ?></header>

// index.php
<?php
require_once 'config.php';
...
<body>
<?php include 'cabecera.php' ?>
</body>

EOS;
?>
        <pre><code><?= htmlspecialchars($sesionesEnMultiplesFicheros, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>


    <h2>Errores habituales en la gestión de sesiones</h2>

    <p>Cuando utilizamos las sesiones de PHP nos podemos encontrarnos algunos errores:</p>
    <ul>
        <li><em lang="en">Warning: session_start(): Session cannot be started after headers have already been sent</em>. Este error es debido a que hemos llamado a <code>session_start()</code> demasiado tarde, en la gestión de la petición. Más concretamente, el error es debido a que hemos empezado a generar HTML como parte la gestión de nuestra petición pero el mecanismo de sesiones de PHP utiliza por defecto cookies que <em>viajan como cabeceras en la respuesta del servidor</em>. También puede ser debido a <a href="https://stackoverflow.com/questions/8028957/how-to-fix-headers-already-sent-error-in-php">otros problemas</a> más extraños como una mala configuración de tu entorno de desarrollo (e.g. codificación UTF-8 con BOM).</li>
        <li><em>Notice: session_start(): Ignoring session_start() because a session is already active</em>. Este error es debido a que estás llamando a <code>session_start()</code> más de una vez en tu código. Para evitar este problema es recomendable sigas la pauta que hemos mostrado en el <a href="#sesion-multiples-archivos">ejemplo de gestión de sesiones</a>.</li>
    </ul>

    <h2>Parámetros de configuración de las sesiones</h2>

    <p>El módulo de gestión de sesiones tiene un conjunto de <a href="https://www.php.net/manual/en/session.configuration.php">parámetros configurables</a>. Algunos de los parámetros que nos pueden interesar configurar son:</p>
    <dl id="session-params">
        <dt>session.name</dt>
        <dd>Permite configurar el nombre de la sesión, que es utilizado como nombre para la cookie asociada a la sesión.</dd>

        <dt>session.cookie_path</dt>
        <dd>Permite configurar el path al que está asociado la cookie de sesión. Este parámetro por defecto toma el valor <code>/</code>, de modo que si cualquier aplicación que esté en tu servidor en diferentes subcarpetas (e.g. <code>/p1</code>, <code>/p2</code>) compartirían la cookie de sesión y, por tanto, compartirían el contenido de la variable <code>$_SESSION</code>, con los problemas asociados.</dd>

        <dt>session.gc_maxlifetime</dt>
        <dd>Permite configurar el número de segundos que al menos la sesión del usuario va a estar disponible, aunque el usuario no esté interactuando con la aplicación.</dd>
    </dl>

    <p>Los parámetros de configuración de la sesión (de hecho cualquier parámetro de configuración de PHP) se puede <a href="../configPHP.php">configurar de varias maneras</a>.</p>

    <section id="sesion-multiples-archivos" class="ejemplo">
        <h6>Configuración e inicialización de la sesión de PHP</h6>
<?php
$configSesion = <<<'EOS'
session_start([
    'cookie_path' => '/practica2', // La sesión sólo afecta a '/practica2'
    'gc_maxlifetime' => 2*60*60 // 2 horas
]);

EOS;
?>
        <pre><code><?= htmlspecialchars($configSesion, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>

    <pre><code>

    </code></pre>
</body>
</html>