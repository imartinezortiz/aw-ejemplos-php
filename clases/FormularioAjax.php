<?php

/**
 * Clase base para los formularios que procesan las peticiones mediante AJAX
 */
abstract class FormularioAjax extends Formulario
{

    /**
     * @var int Código de respuesta HTTP enviado como parte de la respuesta.
     */
    protected $statusCode;

    public function __construct($formId, $opciones=[])
    {
        parent::__construct($formId, $opciones);
    }

    /**
     * Se encarga de orquestar todo el proceso de gestión de un formulario.
     * 
     * El proceso es el siguiente:
     * <ul>
     *   <li>O bien se quiere mostrar el formulario (petición GET)</li>
     *   <li>O bien hay que procesar el formulario (petición POST) y hay dos situaciones:
     *     <ul>
     *       <li>El formulario se ha procesado correctamente y se devuelve un <code>string</code> en {@see Form::procesaFormulario()}
     *           que será la URL a la que se rederigirá al usuario. Se redirige al usuario y se termina la ejecución del script.</li>
     *       <li>El formulario NO se ha procesado correctamente (errores en los datos, datos incorrectos, etc.) y se devuelve
     *           un <code>array</code> con entradas (campo, mensaje) con errores específicos para un campo o (entero, mensaje) si el mensaje
     *           es un mensaje que afecta globalmente al formulario. Se vuelve a generar el formulario pasándole el array de errores.</li> 
     *     </ul>
     *   </li>
     * </ul>
     */
    public function gestiona()
    {
        $datos = &$_POST;
        if (strcasecmp('GET', $this->method) == 0) {
            $datos = &$_GET;
        }
        $this->errores = [];

        if (!$this->formularioEnviado($datos)) {
            return $this->generaFormulario();
        }

        $result = $this->procesaFormulario($datos);
        $esValido = count($this->errores) === 0;
        $this->statusCode = $esValido ? 200 : $this->statusCode ?? 406;

        $response = new stdClass();
        $response->statusCode = $this->statusCode;
        $response->ok = $esValido;
        $response->result = $result;
        if (! $esValido ) {
            $response->errores = $this->generaErroresAjax();
        }

        $json = json_encode($response);
        header_remove();
        http_response_code($this->statusCode);
        //header('Cache-Control: no-cache, no-store, must-revalidate');
        header('Content-type: application/json; charset=utf-8');
        header('Content-Length: '.strlen($json));
        echo $json;
        exit();
    }

    /**
     * Genera la salida que espera la llamada AJAX que ha enviado el formulario.
     *
     * @return string[] Array con los errores de validación.
     */
    protected function generaErroresAjax()
    {
        $result = [];

        foreach($this->errores as $clave => $valor) {
            $error = new stdClass();
            if (is_numeric($clave)) {
                $error->mensaje = $valor;
            } else {
                $error->campo = $clave;
                $error->mensaje = $valor;
            }
            $result[] = $error;
        }

        return $result;
    }
}
