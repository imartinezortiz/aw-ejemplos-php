<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Translation table</title>
    <style>
        table, table td {
            border: 2px solid black;
            text-align: center;
        }

        table {
            border-collapse: collapse;
            width: 80%;
            margin: auto;
        }

        table th {
            background-color: black;
            color: white;
        }
    </style>
</head>
<body>
    <h1>Tablas de traducción de la funcion <a href="https://www.php.net/manual/en/function.get-html-translation-table.php"><code>get_html_translation_table</code></a></h1>
    <p>Utilizando la <code>get_html_translation_table</code> podemos consultar la traducción entre caracteres especiales y entidades que realizan las funciones <code>htmlspecialchars()</code> y <code>htmlentities()</code>. Como puede observarse, la traducción varía bastante, por lo que es recomendable elegir la función más adecuada para cada situación.</p>
    <h2><code>get_html_translation_table(HTML_SPECIALCHARS, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5)</code></h2>
    <table>
        <caption>Traducción de entidades en <code>htmlspecialchars()</code></caption>
        <thead>
            <tr>
                <th>Carácter</th>
                <th>Entidad</th>
            </tr>
        </thead>
        <tbody>
<?php
        $lista = get_html_translation_table(HTML_SPECIALCHARS, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5, 'UTF-8');
        foreach($lista as $key => $value) {
            printf("<tr><td>%s</td><td>%s</td></tr>", htmlspecialchars($key), htmlspecialchars($value));
        }
?>
        </tbody>
    </table>
    <h2><code>get_html_translation_table(HTML_ENTITIES, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5)</code></h2>
</body>
<table>
        <caption>Traducción de entidades en <code>htmlentities()</code></caption>
        <thead>
            <tr>
                <th>Carácter</th>
                <th>Entidad</th>
            </tr>
        </thead>
        <tbody>
<?php
        $lista = get_html_translation_table(HTML_ENTITIES, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5, 'UTF-8');
        foreach($lista as $key => $value) {
            printf("<tr><td>%s</td><td>%s</td></tr>", htmlspecialchars($key), htmlspecialchars($value));
        }
?>
        </tbody>
    </table>
    <footer>
        <p>Versión de PHP: <?= phpversion() ?></p>
        <p>Fecha y hora: <?= date('Y-m-d , H:i:s (\G\M\TP)  ') ?></p>
    </footer>
</html>