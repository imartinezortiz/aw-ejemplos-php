<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../assets/css/estilos.css?20220304" />
    <title>Gestión de peticiones en PHP</title>
</head>
<body>
    <h1>Gestión de peticiones en PHP</h1>

    <h2>Recordatorio sobre páginas / archivos <code>.php</code></h2>

    <p>Recuerda que una página PHP puede contener exclusivamente HTML, o grandes trozos de HTML intercalados con secciones PHP <code>&lt;?php ... ?&gt;</code>. Es decir, los siguientes dos ejemplos son totalmente válidos:</p>
    <section class="ejemplo">
        <h6>Ejemplo de página PHP sólo con HTML</h6>
<?php

$paginaPhpBasica = <<<'EOS'
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
   ...
</body>
</html>
EOS;

?>
        <pre><code><?= htmlspecialchars($paginaPhpBasica, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>
    <section class="ejemplo">
        <h6>Ejemplo de página PHP simple</h6>
<?php

$paginaPhpSimple = <<<'EOS'
<?php

$saludo = 'Hola mundo';

// NOTA: no hay separación entre el cierre de PHP ?> y <!DOCTYPE html>
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $saludo ?></title>
</head>
<body>
   <h1><?= $saludo ?></h1>
</body>
</html>
EOS;

?>
        <pre><code><?= htmlspecialchars($paginaPhpSimple, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
    </section>

    <h2>¿Dónde coloco el PHP?</h2>

    <p>La lógica de gestión en los scripts PHP, en particular en los encargados de gestionar formularios, debe de ejecutarse antes de generar cualquier tipo de salida para el usuario. </p>
    <p>Para lograr este comportamiento, debemos de colocar la lógica de gestión de las peticiones al comienzo del script PHP, como en este ejemplo:</p>
    <section class="ejemplo">
        <h6>Ejemplo de lógica PHP</h6>
<?php

$ejemploForm = <<<'EOS'
<?php
    $parametro = null;
    if (isset($_POST['param1'])) {
        $parametro = (int) $_POST['param1'];
    }
    // Resto de lógica de gestión de la petición o formulario
    ...
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Ejemplo</title>
</head>
<body>
...
EOS;

?>
        <pre><code><?= htmlspecialchars($ejemploForm, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5)?></code></pre>
    </section>

    <h2>Mecanismos útiles de PHP para simplificar la lógica (de formularios)</h2>

    <h3 id="cadenas-multilinea">Cadenas multi-línea</h3>
    <p>Además de la sintaxis que ya hemos visto para definir cadenas en PHP, existen dos maneras adicionales para definir una cadena en PHP que están especialmente recomendadas cuando queremos crear <em>cadenas que se expanden múltiples líneas</em>. Al igual que con la definición de cadenas más básica, existen dos maneras de crear las cadenas multi-línea, una que <a href="https://www.php.net/manual/en/language.types.string.php#language.types.string.parsing">expande variables</a> (y expresiones) y otra que no.</p>
    <p>Las cadenas <a href="https://www.php.net/manual/en/language.types.string.php#language.types.string.syntax.heredoc">heredoc (&lt;&lt;&lt;DELIMITADOR)</a> permite crear cadenas multi-líneas que expanden las variables. Por ejemplo, dado el siguiente código:</p>
    <section class="ejemplo">
        <h6>Ejemplo heredoc</h6>
<?php

$ejemploHeredoc = <<<'EOS'
$nombre = 'Ivan';
$apellido = 'Martinez';
$formulario = <<<DELIM
<form>
    <div>
        <label for="nombreHeredoc">Nombre: </label>
        <input type="text" id="nombreHeredoc" name="nombre" value="$nombre" /></div>
    <div>
        <label for="apellidoHeredoc">Apellido: </label>
        <input type="text" id="apellidoHeredoc" name="apellido" value="$apellido" />
    </div>
</form>
DELIM;
EOS;

?>
        <pre><code><?= htmlspecialchars($ejemploHeredoc, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
        <p>Obtendríamos la siguiente cadena en <code>$formulario</code>:</p>
        <pre><samp><?php
            // XXX: OJO eval() es MUY peligroso (sobre todo cuando se utilizan datos que vienen del usuario) y no se debería de usar. Aquí tiene sentido para generar la salida del ejemplo
            eval("{$ejemploHeredoc};echo htmlspecialchars(\$formulario, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5);")
        ?></samp></pre>

        <p>Interpretándose del siguiente modo la salida:</p>
        <div class="salida"><?php eval("{$ejemploHeredoc};echo \$formulario;") ?></div>
    </section>

    <p>También existe la sintaxis <a href="https://www.php.net/manual/en/language.types.string.php#language.types.string.syntax.nowdoc">nowdoc (&lt;&lt;&lt;&apos;DELIMITADOR&apos;)</a> que no expande variables o expresiones.</p>
    <section class="ejemplo">
        <h6>Ejemplo nowdoc</h6>
<?php

$ejemploNowdoc = <<<'EOS'
$nombre = 'Ivan';
$apellido = 'Martinez';
$formulario = <<<'DELIM'
<form>
    <div>
        <label for="nombreNowdoc">Nombre: </label>
        <input type="text" id="nombreNowdoc" name="nombre" value="$nombre" />
    </div>
    <div>
        <label for="apellidoNowedoc">Apellido: </label>
        <input type="text" id="apellidoNowedoc" name="apellido" value="$apellido" />
    </div>
</form>
DELIM;
EOS;

?>
        <pre><code><?= htmlspecialchars($ejemploNowdoc, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5) ?></code></pre>
        <p>Obtendríamos la siguiente cadena en <code>$formulario</code>:</p>
        <pre><samp><?php
            // XXX: OJO eval() es MUY peligroso (sobre todo cuando se utilizan datos que vienen del usuario) y no se debería de usar. Aquí tiene sentido para generar la salida del ejemplo
            eval("{$ejemploNowdoc};echo htmlspecialchars(\$formulario, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5);")
        ?></samp></pre>
        <p>Interpretándose del siguiente modo la salida:</p>
        <div class="salida"><?php eval("{$ejemploNowdoc};echo \$formulario;") ?></div>
    </section>

    <h4>Simplificación en la el procesado de parámetros de entrada con los operadores <code>?:</code> y <code>??</code></h4>

    <p>Existen un par de operadores que pueden ser de gran utilidad a la hora de procesar la entra del usuario a través de las variables superglobales, ya que simplifican bastante el código. Estos dos operadores son el operador ternario (<a href="https://www.php.net/manual/en/language.operators.comparison.php#language.operators.comparison.ternary"><code>?:</code></a>), también conocido como operador &quot;elvis&quot;, y el operador <a href="https://www.php.net/manual/en/language.operators.comparison.php#language.operators.comparison.coalesce"><code>??</code></a>.</p>

    <p>Un ejemplo típico de código PHP para procesar un parámetro <code>saludos</code> que llega a través de la URL (e.g. <a href="<?= $_SERVER['PHP_SELF'] ?>?saludos=25#ejemplo-elvis">saludos=25</a>):</p>
    <section class="ejemplo" id="ejemplo-elvis">
        <h6>Procesamiento básico de parámetro <code>HTTP GET</code></h6>
<?php

$ejemploElvisAntes = <<<'EOS'
// 1. Damos un valor por defecto al parámetro que sea adecuado
$saludos=0;
if (isset($_GET['saludos'])) {
    // 2. Si está definido lo obtenemos
    $saludos = $_GET['saludos'];
}

echo "Saludos: $saludos";
EOS;
?>
        <pre><code><?= $ejemploElvisAntes ?></code></pre>
        <p>Y una vez ejecutado, el valor sería:</p>
        <pre><samp><?php
            // XXX: OJO eval() es MUY peligroso (sobre todo cuando se utilizan datos que vienen del usuario) y no se debería de usar. Aquí tiene sentido para generar la salida del ejemplo
            echo eval($ejemploElvisAntes)
        ?></samp></pre>
    </section>

    <p>Este ejemplo lo podemos simplificar con el operador ternario, quedando del siguiente modo:</p>
    <section class="ejemplo" id="ejemplo-elvis">
        <h6>Simplificación el código aplicando el operador ternario</h6>
<?php
$ejemploElvisDespues = <<<'EOS'
// Como 1 y 2 en el ejemplo anterior.
$saludos= isset($_GET['saludos']) ? $_GET['saludos'] : 0;

echo "Saludos: $saludos";
EOS;

?>
        <pre><code><?= $ejemploElvisDespues ?></code></pre>
        <p>siendo el resultado de su evaluación el mismo:</p>
        <pre><samp><?php
            // XXX: OJO eval() es MUY peligroso (sobre todo cuando se utilizan datos que vienen del usuario) y no se debería de usar. Aquí tiene sentido para generar la salida del ejemplo
            echo eval($ejemploElvisDespues)
        ?></samp></pre>
    </section>

    <p>Como este patrón de acceso y uso de variables con <code>isset()</code> es muy habitual, con la versión 7.0 de PHP se introdujo el
    operador <a href="https://www.php.net/manual/en/language.operators.comparison.php#language.operators.comparison.coalesce"><code>??</code></a>. Utilizando este nuevo operador el código quedaría finalmente del siguiente modo:</p>
    <section class="ejemplo" id="ejemplo-nullcoalescing">
        <h6>Simplificación aún más el código</h6>
<?php

$ejemploNullCoalescing = <<<'EOS'
// Como en el ejemplo del operador ternario, aún más simplificado
$saludos= $_GET['saludos'] ?? 0;

echo "Saludos: $saludos";
EOS;
?>
        <pre><code><?= $ejemploNullCoalescing ?></code></pre>
        <p>y al igual que en los otros dos casos, el valor de no cambia:</p>
        <pre><samp><?php
            // XXX: OJO eval() es MUY peligroso (sobre todo cuando se utilizan datos que vienen del usuario) y no se debería de usar. Aquí tiene sentido para generar la salida del ejemplo
            echo eval($ejemploNullCoalescing) 
        ?></samp></pre>
    </section>
</body>
</html>
