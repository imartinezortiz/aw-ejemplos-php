<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/estilos.css?20220304" />
    <title>Configuración de PHP</title>
</head>
<body>
    <h1>Configuración de PHP</h1>

    <p> El motor de PHP tiene <a href="https://www.php.net/manual/en/ini.core.php">numerosos parámetros</a> que controlan el comportamiento y la ejecución del motor. Estos parámetros se pueden <a href="https://www.php.net/manual/en/configuration.changes.modes.php">configurar de varias maneras</a>:</p>

    <ol>
        <li><em>En el fichero <code>php.ini</code></em>. Este archivo contiene la configuración por defecto del motor de PHP. <a href="http://localhost/dashboard/phpinfo.php">visitando este enlace de XAMPP</a>, podrás ver en el apartado <em>Loaded Configuration File</em>, la ruta específica al archivo <code>php.ini</code> que está utilizando Apache.</li>
        <li><em><a href="https://www.php.net/manual/en/configuration.changes.php">Desde Apache</a></em>. Puedes utilizar <code>php_flag</code>, <code>php_value</code>, tanto dentro del fichero <code>httpd.conf</code> como en un fichero <code>.htaccess</code> (si se ha habilitado <code>AllowOverride All</code>).</li>
        <li><em>Desde el propio código</em>. PHP ofrece <a href="https://www.php.net/manual/en/ref.info.php">funciones para modificar la configuración del motor</a>, que puedes utilizar en tus scripts y que modifican el comportamiento dinámicamente. En particular, las funciones <a href="https://www.php.net/manual/en/function.ini-set.php"><code>ini_set</code></a> e <a href="https://www.php.net/manual/en/function.ini-get.php"><code>ini_get</code></a> son las que más te pueden interesar. También existen algunos módulos (como el de gestión de sesiones) que ofrecen funciones para poder configurar algunos parámetros.</li>
    </ol>

    <aside class="advertencia">Cada parámetro de configuración PHP puede imponer alguna limitación relacionada el método / lugar donde es posible configurar dicho parámetro. Consulta la <a href="https://www.php.net/manual/en/configuration.changes.modes.php">documentación de PHP</a> para averiguar que métodos de configuración puedes aplicar.</aside>

    <p>Algunos parámetros interesantes son:</p>

    <ul>
        <li><a href="https://www.php.net/manual/en/ini.core.php#ini.upload-max-filesize">upload_max_filesize</a></li>
        <li><a href="https://www.php.net/manual/en/ini.core.php#ini.max-file-uploads">max_file_uploads</a></li>
        <li><a href="https://www.php.net/manual/en/ini.core.php#ini.max-execution-time">max_execution_time</a></li>
        <li><a href="https://www.php.net/manual/en/ini.core.php#ini.memory-limit">memmory_limit</a></li>
        <li><a href="https://www.php.net/manual/en/errorfunc.configuration.php#ini.display-errors">display_errors</a>. Establece si los errores se muestran (se envían como parte de la respuesta del servidor). Este parámetro varía entre entornos de desarrollo (activo) y entornos productivos (desactivado).</li>
        <li><a href="https://www.php.net/manual/en/errorfunc.configuration.php#ini.error-reporting">error_reporting</a>. Permite modificar qué tipo de errores se notifican y cuales se descartan. También suele variar entre entornos.</li>
        <li><a href="https://www.php.net/manual/en/errorfunc.configuration.php#ini.log-errors">log_errorrs</a>. Establece si los errores se registran en un fichero de log dentro del servidor.</li>
        <li><a href="sesiones/index.php#session-params">Los parámetros de gestión de sesiones</a></li>
    </ul>

</body>
</html>